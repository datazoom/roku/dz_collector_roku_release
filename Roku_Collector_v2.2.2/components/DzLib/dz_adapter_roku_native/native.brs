sub Init()
    m.Device = CreateObject("roDeviceInfo")
    m.appInfo = CreateObject("roAppInfo")
    m.global.observeField("input", "onInputReceived")
    m.eventConfig = invalid
    m.debug = false
    m.bufferFillVal = 0
    m.template = {}
    m.template.event = {}
    m.template.user_details = {}
    m.template.Device = {}
    m.template.player = {}
    m.template.video = {}
    m.template.geo_location = {}
    m.template.network = {}
    m.template.cmcd = {}
    m.template.custom = CreateObject("roAssociativeArray")
    m.customSessionMeta = {}
    m.customPlayerMeta = {}
    m.template.event.attributes = {}
    m.template.ad = {}
    m.template.page = {}
    m.template.ops_metadata = {}
    m.oldposition = 0
    m.playerPlay = false
    m.playerPlayRequest = false
    m.playerResume = false
    m.bufferStarted = false
    m.videoPaused = false
    m.playStateTimer = m.top.findNode("playStateTimer")
    m.playStateTimer.ObserveField("fire", "playStateTimerFunction")
    m.playbackTimerContent = m.top.findNode("playbackTimerContent")
    m.playbackTimerContent.ObserveField("fire", "playbackTimerContentFunction")
    m.contentFirstQuartile = true
    m.contentSecondQuartile = true
    m.contentThirdQuartile = true
    m.quartilePresence = false
    m.dataTimer = m.top.findNode("dataTimer")
    m.dataTimer.ObserveField("fire", "dataTimerFunction")
    m.renditionName = ""
    m.renditionBitrate = 0
    m.renditionVideoBitrate = 0
    m.renditionAudioBitrate = 0
    m.streamBitrate = 0
    m.mute = false
    m.defaultMute = false
    m.playCounter = 0
    m.defaultRate = 1
    m.milestonePercent = 0
    m.playerHeight = 0
    m.playerWidth = 0
    m.renditionHeight = 0
    m.renditionWidth = 0
    m.streamBitrate = 0
    m.systemBitrate = 0
    m.pausePoint = 0
    m.sessionStartTimestamp = 0
    m.numberOfErrors = 0
    m.bufferDuration = 0
    m.bufferStartTime = 0
    m.adBufferStartTime = 0
    m.stallStartTime = 0
    m.bufferDurationAds = 0
    m.bufferDurationContent = 0

    m.TSBUFFERSTART = 0
    m.TSSTALLSTART = 0
    m.TSADSTALLSTART = 0
    m.TSLASTHEARTBEAT = 0
    m.TSLASTMILESTONE = 0
    m.TSLASTADHEARTBEAT = 0
    m.TSLASTADMILESTONE = 0
    m.TSPAUSED = 0
    m.TSREQUESTED = 0
    m.TSSTARTED = 0
    m.TSADSTARTED = 0
    m.TSLOADED = 0
    m.TSRENDITIONCHANGE = 0
    m.playerAction = ""
    m.TSADIMPRESSION = 0
    m.TSADBREAKSTART = 0
    m.adSeekStartTime = 0
    m.seekStartTime = 0
    m.networkBandwidth = 0
    m.streamA = 0

    m.fired10 = false
    m.fired25 = false
    m.fired50 = false
    m.fired75 = false
    m.fired90 = false
    m.fired95 = false

    m.tempCustomMeta = {}

    m.adFired10 = false
    m.adFired25 = false
    m.adFired50 = false
    m.adFired75 = false
    m.adFired90 = false
    m.adFired95 = false
    m.lastAdMilestoneTime = 0
    m.lastAdHeartbeatTime = 0
    m.lastAdTime = 0

    m.videoType = "content"
    m.engagementContentStart = 0
    m.engagementContendDuration = 0
    m.oldAdTime = 0
    m.adTime = 0
    m.playContentDuration = 0
    m.adPlaybackDuration = 0
    m.adUrl = ""
    m.adTitle = ""
    m.adServer = ""
    m.adPauseStartTime = 0
    m.pauseDurationAds = 0
    m.adsQV = {}
    m.contentsQV = {}

    m.playbackStallCount = 0
    m.playbackStallCountAds = 0
    m.playbackStallCountContent = 0

    m.playbackStallDuration = 0
    m.playbackStallDurationAds = 0
    m.playbackStallDurationContent = 0
    m.tmpPlaybackTimer = 0
    m.pauseDuration = 0
    m.firstFrameTriggered = false
    m.playbackDurationContent = 0
    m.heartCount = 1
    m.playbackCompleteTriggered = false
    m.playRequestTriggered = false
    m.playbackDurationTotal = 0
    m.eventCount = 0
    m.contentSessionStart = 0
    m.contentRequestCount = 0
    m.numberOfErrorsContent = 0
    m.numberOfContentPlays = 0
    m.numberOfAdsPlayed = 0
    m.firstStart = false
    m.timeSinceLastHeartbeat = 0
    m.contentUrlStop = invalid
    m.contentUrlStart = invalid
    m.playbackDurationContent = 0
    m.playbackDurationTotal = 0
    m.tenths = 00
    m.prevState = "idle"
end sub

' Function to get player Type
function playerType()
    return "native"
end function

' Function to get deviceType
function deviceType()
    return "ott device"
end function

function getMediaType()
    return LCase(m.videoType)
end function

' Function containing Datapoint List
function getLibConfig()
    if m.config = invalid
        m.config = {
            events: {
                EVENTPLAY: "play",
                EVENTPAUSE: "pause",
                EVENTBUFFERING: "buffering",
                EVENTBUFFEREND: "buffer_end",
                EVENTBUFFERSTART: "buffer_start",
                EVENTSTALLSTART: "stall_start",
                EVENTSTALLEND: "stall_end",
                EVENTFIRSTFRAME: "playback_start",
                EVENTPLAYBACKCOMPLETE: "playback_complete",
                EVENTMEDIAREQUEST: "media_request",
                EVENTPLAYING: "playing",
                EVENTRESUME: "resume",
                EVENTPLAYERREADY: "player_ready",
                EVENTCFQ: "Content_First_Quartile",
                EVENTCSQ: "Content_Second_Quartile",
                EVENTCTQ: "Content_Third_Quartile",
                EVENTMUTE: "Mute",
                EVENTUNMUTE: "Unmute",
                EVENTBITRATECHANGE: "Bitrate_Change",
                EVENTADREQUEST: "Ad_Request",
                EVENTADCOMPLETE: "Ad_Complete",
                EVENTADIMPRESSION: "ad_impression",
                EVENTDATAZOOMLOADED: "datazoom_loaded",
                EVENTCUSTOM: "Custom_Event",
                EVENTHEARTBEAT: "heartbeat",
                EVENTCONTENTLOADED: "media_loaded",
                EVENTMILESTONE: "milestone",
                EVENTRENDITIONCHANGE: "rendition_change",
                EVENTSEEKSTART: "seek_start",
                EVENTSEEKEND: "seek_end",
                EVENTERROR: "error",
                EVENTADCLICK: "ad_click",
                EVENTADLOADED: "media_loaded",
                EVENTADPLAY: "playback_start",
                EVENTADERROR: "error",
                EVENTADRENDITIONCHANGE: "Ad_Rendition_Change",
                EVENTADPAUSE: "pause",
                EVENTADRESUME: "resume",
                EVENTADBREAKSTART: "ad_break_start",
                EVENTADBREAKEND: "ad_break_end",
                EVENTADCOMPLETE: "playback_complete",
                EVENTADREQUEST: "media_request",
                EVENTPLAYBTN: "play_btn",
                EVENTSTOP: "stop",
                EVENTQVIEW: "qualified_view",
                EVENTMEDIAOBJECTREQUEST: "media_object_request",

                METAQVTRIGGER: "qualified_view_sec",
                METADURATION: "duration_sec",
                METAIP: "client_ip",
                METACITY: "city",
                METACOUNTRYCODE: "country_code",
                METAREGIONCODE: "region_code",
                METAOS: "os_name",
                METADEVICETYPE: "device_type",
                METADEVICEID: "device_id",
                METADEVICENAME: "device_name",
                METADEVICEMFG: "device_mfg",
                METAOSVERSION: "os_version",
                METAASN: "asn",
                METAASNORG: "asn_org",
                METAISP: "isp",
                METACOUNTRY: "country",
                METAREGION: "region",
                METAZIP: "postal_code",
                METASITEDOMAIN: "site_domain",
                METATIMEZONE: "timezone_name",
                METATIMEZONEOFFSET: "timezone_offset",
                METACONTINENT: "continent",
                METACONTINENTCODE: "continent_code",
                METADISTRICT: "district",

                METACONTROLS: "controls",
                METALOOP: "loop",
                METAREADYSTATE: "readyState",
                METASESSIONVIEWID: "app_session_id",
                METAVIEWID: "content_session_id",
                METALONGITUDE: "longitude",
                METALATITUDE: "latitude",
                METADESCRIPTION: "description",
                METATITLE: "title",
                METAVIDEOTYPE: "media_type",
                METADEFAULTMUTED: "defaultMuted",
                METAISMUTED: "isMuted",
                METASOURCE: "source",
                METACUSTOM: "customMetadata",
                METADEFAULTPLAYBACKRATE: "defaultPlaybackRate",
                METAMILESTONEPERCENT: "milestone_percent",
                METAABSSHIFT: "absShift",
                METASEEKENDPOINT: "seek_end_point",
                METASEEKSTARTPOINT: "seek_start_point",

                METASESSIONSTARTTIMESTAMP: "app_session_start_ts_ms",

                METAPLAYERHEIGHT: "player_height",
                METAPLAYERWIDTH: "player_width",
                METAFRAMERATE: "frameRate",
                METAADVERTISINGID: "advertising_id",
                METAASSETID: "asset_id",
                METAERRORCODE: "error_code",
                METAERRORMSG: "error_msg",
                METAADPOSITION: "ad_position",
                METAUSERAGENT: "user_agent",
                METAPLAYERVERSION: "player_version",
                METADZSDKVERSION: "dz_sdk_version",
                METACONNECTIONTYPE: "connection_type",
                METAEVENTCOUNT: "event_count",
                METASTREAMINGPROTOCOL: "streaming_protocol",
                METASTREAMINGTYPE: "streaming_type",
                METATOTALSTARTUPDURATION: "startup_duration_total_ms",
                METASTARTUPDURATIONCONTENT: "startup_duration_content_ms",
                METASUBTITLES: "subtitles",

                METAADSYSTEM: "ad_system",
                METAADBREAKID: "ad_break_id",
                METAADSESSIONID: "ad_session_id",
                METAADCREATIVEID: "ad_creative_id",
                METAADID: "ad_id",

                FLUXPLAYHEADUPDATE: "playhead_position_sec",
                FLUXBUFFERFILL: "buffer_fill_percent",
                FLUXPLAYERSTATE: "player_state",
                FLUXNUMBEROFVIDEOS: "numberOfVideos",
                FLUXRENDITIONBITRATE: "renditionBitrate",
                FLUXTSBUFFERSTART: "time_since_last_buffer_start_content_ms",
                FLUXTSSTALLSTART: "time_since_last_stall_start_content_ms",
                FLUXTSADSTALLSTART: "time_since_last_stall_start_ad_ms",
                FLUXTSLASTHEARTBEAT: "time_since_last_heartbeat_ms",
                FLUXTSLASTMILESTONE: "time_since_last_milestone_content_ms",
                FLUXTSPAUSED: "time_since_last_pause_ms",
                FLUXTSREQUESTED: "time_since_request_content_ms",
                FLUXTSSTARTED: "time_since_started_content_ms",
                FLUXPLAYBACKDURATION: "playback_duration_ms",
                FLUXNUMBEROFADS: "numberOfAds",
                FLUXBITRATE: "bitrate",
                FLUXVIEWSTARTTIME: "viewStartTimestamp",
                FLUXRENDITIONHEIGHT: "rendition_height",
                FLUXRENDITIONWIDTH: "rendition_width",
                FLUXRENDITIONNAME: "rendition_name",
                FLUXRENDITIONVIDEOBITRATE: "rendition_video_bitrate_kbps",
                FLUXRENDITIONAUDIOBITRATE: "rendition_audio_bitrate_kbps",
                FLUXTSLASTRENDITIONCHANGE: "time_since_last_rendition_change_ms",
                FLUXNUMBEROFERRORS: "num_errors",
                FLUXNUMBEROFERRORSCONTENT: "num_errors_content",
                FLUXTSLASTADMILESTONE: "time_since_last_milestone_ad_ms",
                FLUXTSLASTADHEARTBEAT: "time_since_last_heartbeat_ad_ms",
                FLUXBUFFERDURATION: "buffer_duration_ms",
                FLUXBUFFERDURATIONCONTENT: "buffer_duration_content_ms",
                FLUXBUFFERDURATIONADS: "buffer_duration_ads_ms",
                FLUXENGAGEMENTDURATION: "engagement_duration_ms",
                FLUXENGAGEMENTDURATIONCONTENT: "engagement_duration_content_ms",
                FLUXPLAYBACKDURATIONCONTENT: "playback_duration_content_ms",
                FLUXPLAYBACKSTALLDURATIONCONTENT: "stall_duration_content_ms",
                FLUXPLAYBACKSTALLDURATIONADS: "stall_duration_ad_ms",
                FLUXPLAYBACKSTALLDURATION: "stall_duration_ms",
                FLUXPLAYBACKSTALLCOUNTCONTENT: "stall_count_content",
                FLUXPLAYBACKSTALLCOUNTADS: "playbackStallCountAds",
                FLUXPLAYBACKSTALLCOUNT: "stall_count",
                FLUXNUMBEROFADSPLAYED: "numberOfAdsPlayed",
                FLUXNUMBEROFCONTENTPLAYS: "num_content_plays",
                FLUXNUMBEROFCONTENTREQUEST: "num_requests_content",
                FLUXENGAGEMENTDURATIONADS: "engagementDurationAds",
                FLUXPLAYBACKDURATIONADS: "playback_duration_ads_ms",
                FLUXTSLASTAD: "timeSinceLastAd",
                FLUXTSADBREAKSTART: "timeSinceAdBreakStart",
                FLUXTSADREQUESTED: "timeSinceAdRequested",
                FLUXTSADSTARTED: "timeSinceAdStarted",
                FLUXTSADBUFFERSTART: "timeSinceAdBufferStart",
                FLUXBANDWIDTH: "bandwidth_kbps",
                FLUXTSADSEEKSTART: "timeSinceAdSeekStart",
                FLUXTSSEEKSTART: "time_since_last_seek_start_ms",
                FLUXCONTENTSESSIONSTART: "content_session_start_ts_ms",
                FLUXCURRENTSUBTITLES: "current_subtitles",
                FLUXCURRENTAUDIOTRACK: "current_audio_track",
                FLUXPAUSEDURATION: "pause_duration_ms",
                FLUXPAUSEDURATIONCONTENT: "pause_duration_content_ms",
                FLUXPAUSEDURATIONADS: "pause_duration_ads_ms"
            }
        }
    end if
    return m.config
end function

function ResetAll()
    m.Device = CreateObject("roDeviceInfo")
    m.appInfo = CreateObject("roAppInfo")
    m.global.observeField("input", "onInputReceived")
    m.eventConfig = invalid
    m.bufferFillVal = 0
    m.template = {}
    m.template.event = {}
    m.template.user_details = {}
    m.template.Device = {}
    m.template.player = {}
    m.template.video = {}
    m.template.geo_location = {}
    m.template.network = {}
    m.template.custom = CreateObject("roAssociativeArray")
    m.customSessionMeta = {}
    m.customPlayerMeta = {}
    m.template.event.attributes = {}
    m.template.ad = {}
    m.template.page = {}
    m.template.ops_metadata = {}
    m.oldposition = 0
    m.playerPlay = false
    m.playerPlayRequest = false
    m.playerResume = false
    m.bufferStarted = false
    m.videoPaused = false
    m.playStateTimer = m.top.findNode("playStateTimer")
    m.playStateTimer.ObserveField("fire", "playStateTimerFunction")
    m.playbackTimerContent = m.top.findNode("playbackTimerContent")
    m.playbackTimerContent.ObserveField("fire", "playbackTimerContentFunction")
    m.contentFirstQuartile = true
    m.contentSecondQuartile = true
    m.contentThirdQuartile = true
    m.quartilePresence = false
    m.dataTimer = m.top.findNode("dataTimer")
    m.dataTimer.ObserveField("fire", "dataTimerFunction")
    m.renditionName = ""
    m.renditionBitrate = 0
    m.renditionVideoBitrate = 0
    m.renditionAudioBitrate = 0
    m.streamBitrate = 0
    m.mute = false
    m.defaultMute = false
    m.playCounter = 0
    m.defaultRate = 1
    m.milestonePercent = 0
    m.playerHeight = 0
    m.playerWidth = 0
    m.renditionHeight = 0
    m.renditionWidth = 0
    m.streamBitrate = 0
    m.systemBitrate = 0
    m.pausePoint = 0
    m.sessionStartTimestamp = 0
    m.numberOfErrors = 0
    m.bufferDuration = 0
    m.bufferStartTime = 0
    m.adBufferStartTime = 0
    m.stallStartTime = 0
    m.bufferDurationAds = 0
    m.bufferDurationContent = 0

    m.TSBUFFERSTART = 0
    m.TSSTALLSTART = 0
    m.TSADSTALLSTART = 0
    m.TSLASTHEARTBEAT = 0
    m.TSLASTMILESTONE = 0
    m.TSLASTADHEARTBEAT = 0
    m.TSLASTADMILESTONE = 0
    m.TSPAUSED = 0
    m.TSREQUESTED = 0
    m.TSSTARTED = 0
    m.TSADSTARTED = 0
    m.TSLOADED = 0
    m.TSRENDITIONCHANGE = 0
    m.playerAction = ""
    m.TSADIMPRESSION = 0
    m.TSADBREAKSTART = 0
    m.adSeekStartTime = 0
    m.seekStartTime = 0
    m.networkBandwidth = 0
    m.streamA = 0

    m.fired10 = false
    m.fired25 = false
    m.fired50 = false
    m.fired75 = false
    m.fired90 = false
    m.fired95 = false

    m.tempCustomMeta = {}

    m.adFired10 = false
    m.adFired25 = false
    m.adFired50 = false
    m.adFired75 = false
    m.adFired90 = false
    m.adFired95 = false
    m.lastAdMilestoneTime = 0
    m.lastAdHeartbeatTime = 0
    m.lastAdTime = 0

    m.videoType = "content"
    m.engagementContentStart = 0
    m.engagementContendDuration = 0
    m.oldAdTime = 0
    m.adTime = 0
    m.playContentDuration = 0
    m.adPlaybackDuration = 0
    m.adUrl = ""
    m.adTitle = ""
    m.adServer = ""
    m.numberOfAdsPlayed = 0

    m.playbackStallCount = 0
    m.playbackStallCountAds = 0
    m.playbackStallCountContent = 0

    m.playbackStallDuration = 0
    m.playbackStallDurationAds = 0
    m.playbackStallDurationContent = 0
    m.tmpPlaybackTimer = 0
    m.pauseDuration = 0
    m.firstFrameTriggered = false
    m.playbackDurationContent = 0
    m.heartCount = 1
    m.playbackCompleteTriggered = false
    m.playRequestTriggered = false
    m.playbackDurationTotal = 0
    m.eventCount = 0
    m.contentSessionStart = 0
    m.contentRequestCount = 0
    m.numberOfErrorsContent = 0
    m.numberOfContentPlays = 0
    m.firstStart = false
    m.timeSinceLastHeartbeat = 0
    m.contentUrlStop = invalid
    m.contentUrlStart = invalid
    m.playbackDurationContent = 0
    m.playbackDurationTotal = 0
    m.tenths = 00
end function
' --------------------------- Custom event and meta functions ----------------------------
function generateDatazoomEvent(CustomEvent as String, customEventMeta = invalid)
'    configureEvents()
    playerMetrics()
    if customEventMeta <> invalid
        m.tempCustomMeta.Append(m.template.custom)
        m.template.custom.append(customEventMeta)
        wsSend(getMessageTemplate("custom_" + CustomEvent))
        m.template.custom = {}
        m.template.custom.Append(m.tempCustomMeta)
    else
        wsSend(getMessageTemplate("custom_" + CustomEvent))
    end if
end function

function setDZSessionMeta(key = invalid, value = invalid, customMetadata = invalid)
    if Type(key) = "roAssociativeArray"
        m.customSessionMeta.append(key)
    else if key <> invalid and value <> invalid
        m.customSessionMeta[key] = value
    end if
end function

function setDZPlayerMeta(key = invalid, value = invalid, customMetadata = invalid)
    if Type(key) = "roAssociativeArray"
        m.customPlayerMeta.append(key)
    else if key <> invalid and value <> invalid
        m.customPlayerMeta[key] = value
    end if
end function

function getDZSessionMeta()
    customMetaString = FormatJson(m.customSessionMeta)
    return customMetaString
end function

function setDebug(isDebug = false)
    m.debug = isDebug
end function

function getDZPlayerMeta()
    customMetaString = FormatJson(m.customPlayerMeta)
    return customMetaString
end function

function rmDZSessionMeta()
    m.customSessionMeta = {}
end function

function rmDZPlayerMeta()
    m.customPlayerMeta = {}
end function

function getDZVersion()
    if m.template.page["dz_sdk_version"] <> invalid
    return m.template.page["dz_sdk_version"]
    else
    return ""
    end if
end function

function generateAdEvent(adData = invalid)
    if adData <> invalid
        playerMetrics()
        if m.debug = true
            ? "DZ-Print (adData): "; adData
            if adData.ad <> invalid
                ? "DZ-Print (adData.ad): "; adData.ad
            end if
        end if

        if adData.type = "AdRequest"
            m.videoType = "ad"
            if checkIfMetaConfigured(m.eventConfig.METAADSESSIONID)
                m.template.ad["ad_session_id"] = m.Device.getRandomUUID()
            end if

            if checkIfMetaConfigured(m.eventConfig.METAADBREAKID)
                m.template.ad["ad_break_id"] = m.Device.getRandomUUID()
            end if

            if checkIfMetaConfigured(m.eventConfig.METAADSYSTEM) and adData.adserver <> invalid
                m.template.ad["ad_system"] = adData.ad.adsystem
            end if

            if checkIfMetaConfigured("ad_duration_sec")
                m.template.ad["ad_duration_sec"] = adData.duration
            end if
            if adData.rendersequence <> invalid then m.template.ad["ad_position"] = adData.rendersequence
            m.adBufferStartTime = getCurrentTimestampInMillis()
            m.TSADSTARTED = getCurrentTimestampInMillis()
            if checkIfEventConfigured(m.eventConfig.EVENTMEDIAOBJECTREQUEST)
                cmcdData()
                wsSend(getMessageTemplate(m.eventConfig.EVENTMEDIAOBJECTREQUEST))
                m.template.cmcd = {}
            end  if

            if checkIfEventConfigured(m.eventConfig.EVENTMEDIAREQUEST)
                wsSend(getMessageTemplate(m.eventConfig.EVENTMEDIAREQUEST))
            end  if
        end if
        if adData.type = "PodStart"
            m.videoType = "ad"
            if checkIfMetaConfigured(m.eventConfig.METAADSYSTEM) and adData.adserver <> invalid
                m.template.ad["ad_system"] = adData.ad.adsystem
            end if
            if checkIfMetaConfigured("ad_duration_sec")
                m.template.ad["ad_duration_sec"] = adData.duration
            end if
            if adData.rendersequence <> invalid then m.template.ad["ad_position"] = adData.rendersequence
            m.TSADBREAKSTART = getCurrentTimestampInMillis()
            if checkIfEventConfigured(m.eventConfig.EVENTADBREAKSTART)
                wsSend(getMessageTemplate(m.eventConfig.EVENTADBREAKSTART))
            end if

            if checkIfEventConfigured(m.eventConfig.EVENTCONTENTLOADED)
                wsSend(getMessageTemplate(m.eventConfig.EVENTCONTENTLOADED))
            end if
        end if

        if adData.type = "Impression" ' and checkIfEventConfigured(m.eventConfig.ADIMPRESSION)
            m.adsQV = m.top.adQV
            m.adsMilestones = m.top.adMilestones
            m.videoType = "ad"
            startTotalTimer()
            stopContentTimer()
            m.TSADIMPRESSION = getCurrentTimestampInMillis()

            m.adUrl = adData.ad.streams[0].url
            m.adTitle = adData.Ad.adtitle
            if checkIfEventConfigured(m.eventConfig.EVENTADPLAY)
                wsSend(getMessageTemplate(m.eventConfig.EVENTADPLAY))
            end if

            if checkIfMetaConfigured(m.eventConfig.METAADSYSTEM) and adData.adserver <> invalid
                m.template.ad["ad_system"] = adData.ad.adsystem
            end if

            if checkIfMetaConfigured(m.eventConfig.METAADID)
                m.template.ad["ad_id"] = adData.ad.adid
            end if
            if checkIfMetaConfigured(m.eventConfig.METAADCREATIVEID)
                m.template.ad["ad_creative_id"] = adData.ad.creativeid
            end if

            if checkIfMetaConfigured("ad_duration_sec")
                m.template.ad["ad_duration_sec"] = adData.duration
            end if
            if adData.rendersequence <> invalid then m.template.ad["ad_position"] = adData.rendersequence
            if checkIfEventConfigured(m.eventConfig.EVENTADIMPRESSION)
                wsSend(getMessageTemplate(m.eventConfig.EVENTADIMPRESSION))
            end if
            if checkIfEventConfigured(m.eventConfig.EVENTPLAYING)
                wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYING))
            end if
        end if

        if adData.type = "Complete"
            if checkIfEventConfigured(m.eventConfig.EVENTADCOMPLETE)
                wsSend(getMessageTemplate(m.eventConfig.EVENTADCOMPLETE))
            end if
            if checkIfEventConfigured(m.eventConfig.EVENTADBREAKEND)
                wsSend(getMessageTemplate(m.eventConfig.EVENTADBREAKEND))
            end if
            m.numberOfAdsPlayed = m.numberOfAdsPlayed + 1
            m.template.ad = {}
            m.firstStart = true
            m.engagementContentStart = getCurrentTimestampInMillis()
            m.lastAdTime = getCurrentTimestampInMillis()
            m.adFired10 = false
            m.adFired25 = false
            m.adFired50 = false
            m.adFired75 = false
            m.adFired90 = false
            m.adFired95 = false
            if m.streamType = "Live" and adData.rendersequence = "preroll"
                m.firstStart = true
                m.firstFrameTriggered = false
                m.playRequestTriggered = true
            end if
            m.videoType = "content"
        end if

        if adData.type = "Pause"
            m.adPauseStartTime = getCurrentTimestampInMillis()
            if checkIfEventConfigured(m.eventConfig.EVENTADPAUSE)
                wsSend(getMessageTemplate("pause"))
            end if
            if checkIfEventConfigured(m.eventConfig.EVENTPLAYBTN)
                wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYBTN))
            end if
        end if

        if adData.type = "Resume"
            m.pauseDurationAds = m.pauseDurationAds + (getCurrentTimestampInMillis() - m.adPauseStartTime)
            if checkIfEventConfigured(m.eventConfig.EVENTADPAUSE)
                wsSend(getMessageTemplate(m.eventConfig.EVENTSTALLEND))
            end if
            if checkIfEventConfigured(m.eventConfig.EVENTPLAYBTN)
                wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYBTN))
            end if
            if checkIfEventConfigured(m.eventConfig.EVENTADRESUME)
                wsSend(getMessageTemplate("resume"))
            end if
            if checkIfEventConfigured(m.eventConfig.EVENTPLAY)
                wsSend(getMessageTemplate(m.eventConfig.EVENTPLAY))
            end if
            if checkIfEventConfigured(m.eventConfig.EVENTPLAYING)
                wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYING))
            end if
'          m.numberOfErrors = m.numberOfErrors + 1 
        end if

        if adData.errMsg <> invalid
            ? "*****   Error message: " + adData.errMsg + adData.errCode
            m.template.event.attributes[m.eventConfig.METAERRORCODE] = adData.errCode
            m.template.event.attributes[m.eventConfig.METAERRORMSG] = adData.errMsg
            if checkIfEventConfigured(m.eventConfig.EVENTERROR)
                wsSend(getMessageTemplate(m.eventConfig.EVENTERROR))
            end if
            m.template.event.attributes = {}
        end if
        m.template.ad = {}
'    m.videoType = "content"
    end if

    if adData.duration <> invalid and adData.time <> invalid and adData.time < adData.duration
        if m.adsQV[0] = adData.time
            m.template.event.attributes["qualified_view_sec"] = m.adsQV[0]
            wsSend(getMessageTemplate(m.eventConfig.EVENTQVIEW))
            m.template.event.attributes = {}
            if m.adsQV.count() > 1
                m.adsQV.Shift()
            else
                m.adsQV.Clear()
            end if
        end if

        if m.adsMilestones.Count() > 0
            if adData.time / adData.duration > m.adsMilestones[0] / 100
                m.template.event.attributes["milestone_percent"] = m.adsMilestones[0] / 100
                if checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
                    wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
                end if
'    m.adFired10 = true
                if m.adsMilestones.count() > 1
                    m.adsMilestones.Shift()
                else
                    m.adsMilestones.Clear()
                end if
                m.template.event.attributes = {}
                m.lastAdMilestoneTime = getCurrentTimestampInMillis()
            end if
        end if
    end if
    if m.top.events.interval <> invalid and adData <> invalid
        if adData.time <> invalid and adData.time = 1
            m.adTime = 1
            m.adPlaybackDuration = m.adPlaybackDuration + 1
        else if m.adTime > 0
            m.adTime = m.adTime + 1
            m.adPlaybackDuration = m.adPlaybackDuration + 1
        end if
    end if
end function

' Configure maximum possible events needed to be collected from player.
sub configureEvents()
    m.player = m.top.playerInit.player
'    playStateTimerFunction()
' ============ RESET PLAYER TIMERS ===================
    m.firstFrameTriggered = false
    m.playRequestTriggered = false
    m.firstStart = false
    m.heartbeatTimer = invalid
    m.playbackTimer = invalid
    m.playbackDurationTotal = 0
    m.playbackDurationContent = 0
    m.heartCount = 1
    m.bufferDurationContent = 0
    m.bufferDuration = 0
' =====================================================

'    m.playStateTimer.control = "start"
    m.player.observeField("position", "OnHeadPositionChange")
    m.player.observeField("bufferingStatus", "OnBufferingStatusChange")
    m.player.observeField("state", "OnVideoPlayerStateChange")
    m.player.observeField("timedMetaData", "timedMetaDataChanged")
    m.player.observeField("control", "playerControlChanged")
    m.player.observeField("input", "onInputReceived")
    m.defaultMute = m.player.mute
    m.dataTimer.control = "start"
    m.defaultRate = 1
    m.numberOfErrors = 0
    m.displayMode = m.Device.getDisplayMode()
    m.displaySize = m.Device.getDisplaySize()
    m.playerHeight = m.displaySize.h
    m.playerWidth = m.displaySize.w
end sub

sub timedMetaDataChanged()
end sub

' Checks if specific data points is available or not
function dataPointValidation()
    if checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
        m.quartilePresence = true
    end if
end function

function playerControlChanged()
end function

' Player callback method on each seconds. Use to get the user current position in the playback. 
sub OnHeadPositionChange()
    if m.top.events <> invalid
        if m.top.events.flux_data <> invalid
            fluxAvail = false
            for each fluxType in m.top.events.flux_data
                fluxAvail = true
            end for
            if fluxAvail
                if ((m.player.position - m.oldposition) < 0)
                    m.oldposition = 0
                end if
                m.oldposition = m.player.position
                fluxMetricsData()
                if m.syslog <> invalid
                end if
            end if
        end if
    end if
    ' Calls base to set session time
    callSetSessionTime()
end  sub

function cmcdData()
    m.template.cmcd = {}
    if checkIfMetaConfigured("v")
        m.template.cmcd["v"] = "1"
    end if
    if checkIfMetaConfigured("nor")
        if m.player <> invalid
            if m.player.streamingSegment <> invalid
                m.template.cmcd["nor"] = m.player.streamingSegment.Path
            end if
        end if
    end if

    if checkIfMetaConfigured("rtp")
        if m.player.streamInfo <> invalid
            if m.player.streamInfo.streamBitrate <> invalid or m.player.streamInfo.streamBitrate <> 0
                m.template.cmcd["rtp"] = m.player.streamInfo.measuredBitrate / 1000
            end if
        end if
    end if
    if checkIfMetaConfigured("tb")
        if m.player.streamInfo <> invalid
            if m.player.streamInfo.streamBitrate <> invalid or m.player.streamInfo.streamBitrate <> 0
                m.template.cmcd["tb"] = m.player.streamInfo.streamBitrate / 1000
            end if
        end if
    end if
' ========   
    if checkIfMetaConfigured("rid")
        m.template.cmcd["rid"] = m.Device.getRandomUUID()
    end if
    if checkIfMetaConfigured("cid")
        m.template.cmcd["cid"] = m.Device.getRandomUUID()
    end if
    if checkIfMetaConfigured("sid")
        m.template.cmcd["sid"] = getAppSessionId()
    end if
    if m.streamType <> "Live"
        if checkIfMetaConfigured("st")
            m.template.cmcd["st"] = "v"
        end if
    else
        m.template.cmcd["st"] = "l"
    end if
    if checkIfMetaConfigured("sf")
        if m.player.streamingFormat <> invalid and m.player.streamingFormat <> ""
            if m.player.streamingFormat = "mpeg" or m.player.streamingFormat = "dash"
                m.template.cmcd["sf"] = "d"
            end if
            if m.player.streamingFormat = "live"
                m.template.cmcd["sf"] = "h"
            end if
            if m.player.streamingFormat = "hls"
                m.template.cmcd["sf"] = "h"
            end if
            if m.player.streamingFormat = ""
                m.template.cmcd["sf"] = "o"
            end if
        end if
    end if
    if checkIfMetaConfigured("ot")
        if m.player.streamingFormat <> invalid and m.player.streamingFormat <> ""
            if m.player.streamingFormat = "mpeg"
                m.template.cmcd["ot"] = "av"
            end if
            if m.player.streamingFormat = "live"
                m.template.cmcd["ot"] = "av"
            end if
            if m.player.streamingFormat = "hls"
                m.template.cmcd["ot"] = "m"
            end if
            if m.player.streamingFormat = "dash"
                m.template.cmcd["ot"] = "m"
            end if
        end if
    end if
    if checkIfMetaConfigured("d")
        if m.player.position <> invalid and m.player.duration <> invalid and m.streamType <> "Live"
            m.template.cmcd["d"] = m.player.duration
        end if
    end if
    if m.player.streamInfo <> invalid
        if m.player.streamInfo.streamBitrate <> invalid or m.player.streamInfo.streamBitrate <> 0
            if checkIfMetaConfigured("br")
                m.template.cmcd["br"] = m.player.streamInfo.streamBitrate / 1000
            end if
            if checkIfMetaConfigured("mtp")
                m.template.cmcd["mtp"] = m.player.streamInfo.measuredBitrate / 1000
            end if
        end if
    end if
end function

function fluxMetricsData()
    m.template.event.metrics = {}
    if checkIfFluxConfigured(m.eventConfig.FLUXPLAYHEADUPDATE)
        playHead = 0
        if m.player <> invalid
            if m.player.position <> invalid
                playHead = m.player.position
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXPLAYHEADUPDATE] = playHead
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKDURATION)
        if m.playbackDurationTotal > 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATION] = m.playbackDurationTotal
        else
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATION] = 0
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXPAUSEDURATION)
        if m.playbackDurationTotal > 0
            m.template.event.metrics[m.eventConfig.FLUXPAUSEDURATION] = m.pauseDuration
        else
            m.template.event.metrics[m.eventConfig.FLUXPAUSEDURATION] = 0
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKDURATIONCONTENT)
        if m.playbackDurationContent > 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATIONCONTENT] = m.playbackDurationContent
        else
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATIONCONTENT] = 0
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXPAUSEDURATIONCONTENT)
        if m.playbackDurationTotal > 0
            m.template.event.metrics[m.eventConfig.FLUXPAUSEDURATIONCONTENT] = m.pauseDuration
        else
            m.template.event.metrics[m.eventConfig.FLUXPAUSEDURATIONCONTENT] = 0
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKDURATIONADS)
        if m.adPlaybackDuration > 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATIONADS] = m.adPlaybackDuration * 1000
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXPAUSEDURATIONADS)
        if m.playbackDurationTotal > 0
            m.template.event.metrics[m.eventConfig.FLUXPAUSEDURATIONADS] = m.pauseDurationAds
        else
            m.template.event.metrics[m.eventConfig.FLUXPAUSEDURATIONADS] = 0
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXBUFFERDURATION)
        if m.bufferDuration > 0
            m.template.event.metrics[m.eventConfig.FLUXBUFFERDURATION] = m.bufferDuration
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXBUFFERDURATIONCONTENT)
        if m.bufferDurationContent > 0
            m.template.event.metrics[m.eventConfig.FLUXBUFFERDURATIONCONTENT] = m.bufferDurationContent
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXBUFFERDURATIONADS)
        if m.bufferDurationAds > 0
            m.template.event.metrics[m.eventConfig.FLUXBUFFERDURATIONADS] = m.bufferDurationContentAds
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXBUFFERFILL)
        m.template.event.metrics[m.eventConfig.FLUXBUFFERFILL] = m.bufferFillVal
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXPLAYERSTATE)
        playerState = ""
        if m.player <> invalid
            if m.player.state <> invalid
                playerState = m.player.state
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXPLAYERSTATE] = playerState
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFVIDEOS)
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFVIDEOS] = Val(getNoOfVideos())
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFERRORS)
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFERRORS] = m.numberOfErrors
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFERRORSCONTENT)
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFERRORSCONTENT] = m.numberOfErrors
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFADS)
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFADS] = 1
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFADSPLAYED) and m.numberOfAdsPlayed <> invalid
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFADSPLAYED] = m.numberOfAdsPlayed
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONBITRATE)
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONBITRATE] = m.renditionBitrate
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONVIDEOBITRATE)
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONVIDEOBITRATE] = m.renditionVideoBitrate
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONAUDIOBITRATE)
        if m.player <> invalid
            if m.player.streamingSegment <> invalid
                if m.player.streamingSegment.segType = 1
                    m.renditionAudioBitrate = m.player.streamingSegment.segBitrateBps
                end if
            else
                m.renditionAudioBitrate = m.streamBitrate
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONAUDIOBITRATE] = m.renditionAudioBitrate
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONNAME)
        if m.player <> invalid
            if m.player.downloadedSegment <> invalid
                if m.player.downloadedSegment.segUrl <> invalid
                    m.renditionName = Str(m.player.downloadedSegment.height) + "p"
'                        m.renditionName = m.player.downloadedSegment.segUrl
                end if
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONNAME] = m.renditionName
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONWIDTH)
        if m.player <> invalid
            if m.player.downloadedSegment <> invalid
                if m.player.downloadedSegment.width > 0
                    m.renditionWidth = m.player.downloadedSegment.width
                else
                    m.renditionWidth = m.playerWidth
                end if
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONWIDTH] = m.renditionWidth
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONHEIGHT)
        if m.player <> invalid
            if m.player.downloadedSegment <> invalid
                if m.player.downloadedSegment.height > 0
                    m.renditionHeight = m.player.downloadedSegment.height
                else
                    m.renditionHeight = m.playerHeight
                end if
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONHEIGHT] = m.renditionHeight
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXBITRATE)
        if m.player <> invalid
            if m.player.state <> invalid
                bitrate = m.renditionBitrate
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXBITRATE] = m.renditionBitrate
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXBANDWIDTH)
        m.template.event.metrics[m.eventConfig.FLUXBANDWIDTH] = m.networkBandwidth
    end if

    ' Time Since flux metrics    
    if checkIfFluxConfigured(m.eventConfig.FLUXTSBUFFERSTART)
        if m.TSBUFFERSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXTSBUFFERSTART] = m.TSBUFFERSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXTSBUFFERSTART] = getCurrentTimestampInMillis() - m.bufferStartTime
        end if
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXTSADBUFFERSTART)
        if m.adBufferStartTime = 0
            m.template.event.metrics[m.eventConfig.FLUXTSADBUFFERSTART] = m.adBufferStartTime
        else
            m.template.event.metrics[m.eventConfig.FLUXTSADBUFFERSTART] = getCurrentTimestampInMillis() - m.adBufferStartTime
        end if
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXTSSTALLSTART)
        if m.TSSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXTSSTALLSTART] = m.TSSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXTSSTALLSTART] = getCurrentTimestampInMillis() - m.TSSTALLSTART
        end if
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXTSADSTALLSTART)
        if m.TSADSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXTSADSTALLSTART] = m.TSADSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXTSADSTALLSTART] = getCurrentTimestampInMillis() - m.TSADSTALLSTART
        end if
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXTSLASTHEARTBEAT)
        if m.heartbeatTimer <> invalid
            m.timeSinceLastHeartbeat = m.heartbeatTimer.TotalMilliseconds()
            if m.timeSinceLastHeartbeat = 0
                m.template.event.metrics[m.eventConfig.FLUXTSLASTHEARTBEAT] = m.timeSinceLastHeartbeat
            else
                m.template.event.metrics[m.eventConfig.FLUXTSLASTHEARTBEAT] = m.timeSinceLastHeartbeat' m.heartbeatTimer.TotalMilliseconds()'getCurrentTimestampInMillis() - m.TSLASTHEARTBEAT
            end if
        end if
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXTSLASTADHEARTBEAT)
        if m.lastAdHeartbeatTime = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTADHEARTBEAT] = m.lastAdHeartbeatTime
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTADHEARTBEAT] = getCurrentTimestampInMillis() - m.lastAdHeartbeatTime
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXTSLASTMILESTONE)
        if m.TSLASTMILESTONE = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTMILESTONE] = m.TSLASTMILESTONE
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTMILESTONE] = getCurrentTimestampInMillis() - m.TSLASTMILESTONE
        end if
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXTSLASTADMILESTONE)
        if m.lastAdMilestoneTime = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTADMILESTONE] = m.lastAdMilestoneTime
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTADMILESTONE] = getCurrentTimestampInMillis() - m.lastAdMilestoneTime
        end if
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXTSLASTAD)
        if m.lastAdTime = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTAD] = m.lastAdTime
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTAD] = getCurrentTimestampInMillis() - m.lastAdTime
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXTSPAUSED)
        if m.TSPAUSED = 0
            m.template.event.metrics[m.eventConfig.FLUXTSPAUSED] = m.TSPAUSED
        else
            m.template.event.metrics[m.eventConfig.FLUXTSPAUSED] = getCurrentTimestampInMillis() - m.TSPAUSED
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXTSREQUESTED)
        if m.TSREQUESTED = 0
            m.template.event.metrics[m.eventConfig.FLUXTSREQUESTED] = m.TSREQUESTED
        else
            m.template.event.metrics[m.eventConfig.FLUXTSREQUESTED] = getCurrentTimestampInMillis() - m.TSREQUESTED
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFCONTENTREQUEST)
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFCONTENTREQUEST] = m.contentRequestCount
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFCONTENTPLAYS)
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFCONTENTPLAYS] = m.numberOfContentPlays
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXVIEWSTARTTIME)
        if m.TSREQUESTED = 0
            m.template.event.metrics[m.eventConfig.FLUXVIEWSTARTTIME] = m.TSREQUESTED
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXTSSTARTED)
        if m.TSSTARTED = 0
            m.template.event.metrics[m.eventConfig.FLUXTSSTARTED] = m.TSSTARTED
        else
            m.template.event.metrics[m.eventConfig.FLUXTSSTARTED] = getCurrentTimestampInMillis() - m.TSSTARTED
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXENGAGEMENTDURATIONCONTENT)
        if m.TSSTARTED = 0
            m.template.event.metrics[m.eventConfig.FLUXENGAGEMENTDURATIONCONTENT] = m.TSSTARTED
        else
            m.template.event.metrics[m.eventConfig.FLUXENGAGEMENTDURATIONCONTENT] = getCurrentTimestampInMillis() - m.TSSTARTED
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXENGAGEMENTDURATIONADS)
        if m.TSADSTARTED = 0
            m.template.event.metrics[m.eventConfig.FLUXENGAGEMENTDURATIONADS] = m.TSADSTARTED
        else
            m.template.event.metrics[m.eventConfig.FLUXENGAGEMENTDURATIONADS] = getCurrentTimestampInMillis() - m.TSADSTARTED
        end if
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXTSADREQUESTED)
        if m.TSADSTARTED = 0
            m.template.event.metrics[m.eventConfig.FLUXTSADREQUESTED] = m.TSADSTARTED
        else
            m.template.event.metrics[m.eventConfig.FLUXTSADREQUESTED] = getCurrentTimestampInMillis() - m.TSADSTARTED
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXTSADBREAKSTART)
        if m.TSADBREAKSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXTSADBREAKSTART] = m.TSADBREAKSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXTSADBREAKSTART] = getCurrentTimestampInMillis() - m.TSADBREAKSTART
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXTSADSTARTED)
        if m.TSADIMPRESSION = 0
            m.template.event.metrics[m.eventConfig.FLUXTSADSTARTED] = m.TSADIMPRESSION
        else
            m.template.event.metrics[m.eventConfig.FLUXTSADSTARTED] = getCurrentTimestampInMillis() - m.TSADIMPRESSION
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXENGAGEMENTDURATION)
        m.TSLOADED = getSessionStartTimestamp()
        if m.TSLOADED = 0
            m.template.event.metrics[m.eventConfig.FLUXENGAGEMENTDURATION] = m.TSLOADED
        else
            m.template.event.metrics[m.eventConfig.FLUXENGAGEMENTDURATION] = getCurrentTimestampInMillis() - m.TSLOADED
        end if
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXTSSEEKSTART)
        if m.adSeekStartTime = 0
            m.template.event.metrics[m.eventConfig.FLUXTSADSEEKSTART] = m.adSeekStartTime
        else
            m.template.event.metrics[m.eventConfig.FLUXTSADSEEKSTART] = getCurrentTimestampInMillis() - m.adSeekStartTime
        end if
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXTSSEEKSTART)
        if m.seekStartTime = 0
            m.template.event.metrics[m.eventConfig.FLUXTSSEEKSTART] = m.seekStartTime
        else
            m.template.event.metrics[m.eventConfig.FLUXTSSEEKSTART] = getCurrentTimestampInMillis() - m.seekStartTime
        end if
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXCONTENTSESSIONSTART)
        m.template.event.metrics[m.eventConfig.FLUXCONTENTSESSIONSTART] = m.contentSessionStart
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXTSLASTRENDITIONCHANGE)
        if m.TSRENDITIONCHANGE = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTRENDITIONCHANGE] = m.TSRENDITIONCHANGE
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTRENDITIONCHANGE] = getCurrentTimestampInMillis() - m.TSRENDITIONCHANGE
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKSTALLCOUNT)
        if m.TSSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLCOUNT] = m.TSSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLCOUNT] = m.playbackStallCountAds + m.playbackStallCountContent
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKSTALLCOUNTCONTENT)
        if m.TSSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLCOUNTCONTENT] = m.TSSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLCOUNTCONTENT] = m.playbackStallCountContent
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKSTALLCOUNTADS)
        if m.TSADSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLCOUNTADS] = m.TSADSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLCOUNTADS] = m.playbackStallCountAds
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKSTALLDURATION)
        if m.TSSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLDURATION] = m.TSSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLDURATION] = m.playbackStallDurationContent + m.playbackStallDurationAds
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKSTALLDURATIONCONTENT)
        if m.TSSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLDURATIONCONTENT] = m.TSSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLDURATIONCONTENT] = m.playbackStallDurationContent
        end if
    end if

    if checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKSTALLDURATIONADS)
        if m.TSADSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLDURATIONADS] = m.TSADSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLDURATIONADS] = m.playbackStallDurationAds
        end if
    end if
    if m.player <> invalid
        if checkIfFluxConfigured(m.eventConfig.FLUXCURRENTSUBTITLES)
            if m.player.currentSubtitleTrack <> invalid or m.player.currentSubtitleTrack <> ""
                m.template.event.metrics[m.eventConfig.FLUXCURRENTSUBTITLES] = m.player.currentSubtitleTrack
            else
                m.template.event.metrics[m.eventConfig.FLUXCURRENTSUBTITLES] = "default"
            end if
        end if
    end if
    if checkIfFluxConfigured(m.eventConfig.FLUXCURRENTAUDIOTRACK)
        if m.player <> invalid
            if m.player.currentAudioTrack <> invalid or m.player.currentAudioTrack <> ""
                m.template.event.metrics[m.eventConfig.FLUXCURRENTAUDIOTRACK] = m.player.currentAudioTrack
            else
                m.template.event.metrics[m.eventConfig.FLUXCURRENTAUDIOTRACK] = "default"
            end if
        end if
    end if
end function

' Sets playheadPosition
function playerMetrics()
    m.template.event.metrics = {}
    ' Change this variable value to set whether fluxdata values should be sent with events or not
    callFlexMetrics = true
    if callFlexMetrics
        fluxMetricsData()
    else
        playHead = 0
        if checkIfFluxConfigured(m.eventConfig.FLUXPLAYHEADUPDATE)
            if m.player <> invalid
                if m.player.position <> invalid
                    playHead = m.player.position
                end if
            end if
        end if
        m.template.event.metrics["playheadPosition"] = playHead
    end if
end function

sub resetContentProperties()
    m.playbackDurationTotal = 0
    m.playbackDurationContent = 0
    m.heartCount = 1
    m.bufferDurationContent = 0
    m.bufferDuration = 0
    m.bufferFillVal = 0
    m.playbackStallCount = 0
    m.playbackStallCountAds = 0
    m.playbackStallCountContent = 0
    m.playbackStallDuration = 0
    m.playbackStallDurationAds = 0
    m.playbackStallDurationContent = 0
    m.renditionBitrate = 0
    m.renditionVideoBitrate = 0
    m.renditionAudioBitrate = 0
    m.streamBitrate = 0
    m.TSBUFFERSTART = 0
    m.TSSTALLSTART = 0
    m.TSADSTALLSTART = 0
    m.TSLASTHEARTBEAT = 0
    m.TSLASTMILESTONE = 0
    m.TSLASTADHEARTBEAT = 0
    m.TSLASTADMILESTONE = 0
    m.TSPAUSED = 0
    m.TSREQUESTED = 0
    m.TSSTARTED = 0
    m.TSADSTARTED = 0
    m.TSLOADED = 0
    m.TSRENDITIONCHANGE = 0
    m.TSADIMPRESSION = 0
    m.TSADBREAKSTART = 0
    m.adSeekStartTime = 0
    m.seekStartTime = 0
    m.networkBandwidth = 0
    m.streamA = 0
    m.template.user_details.content_session_id = ""
    if checkIfMetaConfigured(m.eventConfig.METAASSETID)
        m.template.video.asset_id = ""
    end if
end sub

' Player callback method for buffer status changes.
sub OnBufferingStatusChange()
    if m.player.bufferingStatus <> invalid
        m.bufferFillVal = m.player.bufferingStatus.percentage
        if (m.bufferFillVal >= 95)
            if m.bufferStarted
                m.bufferStarted = false
                playerMetrics()
                if checkIfEventConfigured(m.eventConfig.EVENTBUFFEREND)
                    wsSend(getMessageTemplate(m.eventConfig.EVENTBUFFEREND))
                    m.bufferEnd = true
                end if
                m.TSBUFFERSTART = getCurrentTimestampInMillis() - m.bufferStartTime
                m.bufferDuration = m.bufferDuration + m.TSBUFFERSTART
                if m.videoType = "ad"
                    m.bufferDurationAds = m.bufferDurationAds + m.TSBUFFERSTART
                end if
                if m.videoType = "content"
                    m.bufferDurationContent = m.bufferDurationContent + m.TSBUFFERSTART
                end if
                if checkIfEventConfigured(m.eventConfig.EVENTSTALLEND)
                    wsSend(getMessageTemplate(m.eventConfig.EVENTSTALLEND))
                    if m.videoType = "content"
                        m.playbackStallCountContent = m.playbackStallCountContent + 1
                        m.playbackStallDurationContent = m.playbackStallDurationContent + (getCurrentTimestampInMillis() - m.bufferStartTime)
                    end if

                    if m.videoType = "ad"
                        m.playbackStallCountAds = m.playbackStallCountAds + 1
                        m.playbackStallDurationAds = m.playbackStallDurationAds + (getCurrentTimestampInMillis() - m.bufferStartTime)
                    end if
                end if
            end if
        end if
    end if
end sub

' Function to set resume flag
function setResume()
    m.playerResume = true
end function
' Player state change callback method.
sub OnVideoPlayerStateChange(event as Object)
    state = event.getData()
    if m.player <> invalid
        if m.debug = true
            ? "DZ-Print - Player State:";state
        end if
        m.contentsMilestones = m.top.contentMilestones
        if m.player.control <> invalid
            if m.debug = true
            ? "DZ-Print - Player Control:"; m.player.control
            end if
            if m.player.control <> m.playerAction
                m.playerAction = m.player.control
                if LCase(m.playerAction) = "prebuffer" or LCase(m.playerAction) = "play"
                    playerMetrics()
                    m.TSREQUESTED = getCurrentTimestampInMillis()
                    m.template.event.metrics[m.eventConfig.FLUXTSLASTHEARTBEAT] = 0
                    m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATIONCONTENT] = 0
                    m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATION] = 0
                    m.template.video[m.eventConfig.METADURATION] = 0
'                    resetContentProperties()
                    if checkIfEventConfigured(m.eventConfig.EVENTPLAYERREADY)
                        wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYERREADY))
                    end if
                    m.playbackCompleteTriggered = false
                    m.startupTimer = CreateObject("roTimespan")
                end if
            end if
        end if

        playerMetrics()

        if state = "none"
            playerMetrics()
            m.TSREQUESTED = getCurrentTimestampInMillis()
            m.template.event.metrics[m.eventConfig.FLUXTSLASTHEARTBEAT] = 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATIONCONTENT] = 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATION] = 0
            m.template.video[m.eventConfig.METADURATION] = 0
            resetContentProperties()
            if checkIfEventConfigured(m.eventConfig.EVENTPLAYERREADY)
                wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYERREADY))
            end if
            m.playbackCompleteTriggered = false
            m.startupTimer = CreateObject("roTimespan")
            m.prevState = m.player.state
        end if
        if state = "buffering"
            if m.prevState = "idle"
                resetContentProperties()
            end if
            m.videoType = "content"
            if m.player.content <> invalid
                if m.player.content.url <> invalid
                    if checkIfEventConfigured(m.eventConfig.EVENTMEDIAOBJECTREQUEST)
                        cmcdData()
                        wsSend(getMessageTemplate(m.eventConfig.EVENTMEDIAOBJECTREQUEST))
                        m.template.cmcd = {}
                    end  if

                    if checkIfEventConfigured(m.eventConfig.EVENTMEDIAREQUEST)
                        wsSend(getMessageTemplate(m.eventConfig.EVENTMEDIAREQUEST))
                    end if

                    m.contentSessionStart = getCurrentTimestampInMillis()
                    m.contentUrlStart = m.player.content.url
                else
'        m.firstStart = false
                end if
            end if

            if m.contentUrlStart <> m.contentUrlStop
                if m.videoType = "content" or m.videoType = ""
                    resetContentProperties()
                    if checkIfMetaConfigured(m.eventConfig.METAVIEWID)
                        m.template.user_details[m.eventConfig.METAVIEWID] = m.Device.getRandomUUID()
                    end if
                    if checkIfMetaConfigured(m.eventConfig.METAASSETID)
                        m.template.video.asset_id = getAssetID()
                    end if

                    m.firstFrameTriggered = false
                    m.playRequestTriggered = false
                    m.firstStart = false

                    m.heartbeatTimer = invalid
                    m.playbackTimer = invalid
                    stopTotalTimer()

                    m.playbackDurationTotal = 0
                    m.playbackDurationContent = 0
                    m.heartCount = 1
                    m.bufferDurationContent = 0
                    m.bufferDuration = 0

                    m.contentSessionStart = getCurrentTimestampInMillis()
                end if

                m.bufferDurationContent = 0
                m.bufferDuration = 0

                if checkIfEventConfigured(m.eventConfig.EVENTCONTENTLOADED)
                    wsSend(getMessageTemplate(m.eventConfig.EVENTCONTENTLOADED))
                end if

                m.playRequestTriggered = true
                m.firstStart = true

                m.bufferDurationContent = 0
                m.contentRequestCount = m.contentRequestCount + 1
                m.heartCount = 1
                m.TSSTARTED = getCurrentTimestampInMillis()
            end if
            m.prevState = m.player.state
        end if

        if state = "playing"
            startTotalTimer()
            startContentTimer()
            m.videoType = "content"

            if m.player.content <> invalid
                if m.player.content.url <> invalid
                    m.contentUrlStart = m.player.content.url
                end if
            else
                m.firstStart = false
            end if

            if m.contentUrlStart <> m.contentUrlStop or m.firstFrameTriggered = false
'            resetContentProperties()
'            if checkIfMetaConfigured(m.eventConfig.METAVIEWID)
'               m.template.user_details[m.eventConfig.METAVIEWID] = m.device.GetRandomUUID()
'            end if
'            If checkIfMetaConfigured(m.eventConfig.METAASSETID)
'               m.template.video.asset_id = getAssetId()
'            end if

                m.numberOfContentPlays = m.numberOfContentPlays + 1
                m.contentSessionStart = getCurrentTimestampInMillis()
                m.startupDurationContent = m.startupTimer.TotalMilliseconds()
                m.template.event.attributes[m.eventConfig.METASTARTUPDURATIONCONTENT] = m.startupDurationContent
                startupDuration = m.startupTimer.TotalMilliseconds()
                m.template.event.attributes[m.eventConfig.METATOTALSTARTUPDURATION] = startupDuration

                if checkIfEventConfigured(m.eventConfig.EVENTFIRSTFRAME)
                    wsSend(getMessageTemplate(m.eventConfig.EVENTFIRSTFRAME))
                end if

                m.template.event.attributes = {}
                m.firstFrameTriggered = true
                m.contentsQV = m.top.contentQV
            end if

            if checkIfEventConfigured(m.eventConfig.EVENTPLAYBTN)
                wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYBTN))
            end if

            if m.player.position <> invalid
                m.resumePoint = m.player.position
            end if
            m.playCounter = m.playCounter + 1

            if m.playCounter > 1
                setResume()
            end if

            m.videoPaused = false
            ' =============== Resume method ========================
            if m.playerResume
                m.resumePoint = m.player.position
                if Abs(m.pausePoint - m.resumePoint) > 1
                    m.pauseTimer = CreateObject("roTimespan")
                    m.pauseTimer.Mark()
                    ' ==================== SEEK METHODS ====================
                    if checkIfEventConfigured(m.eventConfig.EVENTSEEKSTART)
                        if m.videoType = "ad"
                            m.adSeekStartTime = getCurrentTimestampInMillis()
                            m.template.event.attributes[m.eventConfig.METASEEKSTARTPOINT] = m.adSeekStartTime
                            wsSend(getMessageTemplate(m.eventConfig.EVENTADSEEKSTART))
                            m.template.event.attributes = {}
                        end if
                    end if
                    if checkIfEventConfigured(m.eventConfig.EVENTSEEKSTART)
                        m.seekStartTime = getCurrentTimestampInMillis()
                        m.template.event.attributes[m.eventConfig.METASEEKSTARTPOINT] = m.seekStartTime
                        wsSend(getMessageTemplate(m.eventConfig.EVENTSEEKSTART))
                        m.template.event.attributes = {}
                    end if
                    stopTotalTimer()
                    stopContentTimer()

                    if checkIfEventConfigured(m.eventConfig.EVENTSEEKEND)
                        m.template.event.attributes[m.eventConfig.METASEEKSTARTPOINT] = m.pausePoint * 1000
                        m.template.event.attributes[m.eventConfig.METASEEKENDPOINT] = m.player.position * 1000
                        wsSend(getMessageTemplate(m.eventConfig.EVENTSEEKEND))
                        m.template.event.attributes = {}
                    end if
                end if

                if m.pauseTimer <> invalid
                    m.pausedTime = m.pauseTimer.TotalMilliseconds()
                    m.pauseDuration = m.pauseDuration + m.pausedTime
                    m.pauseTimer = invalid
                end if
                playerMetrics()
                if checkIfEventConfigured(m.eventConfig.EVENTPLAYBTN)
                    wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYBTN))
                end if
                if checkIfEventConfigured(m.eventConfig.EVENTRESUME)
                    wsSend(getMessageTemplate(m.eventConfig.EVENTRESUME))
                end if

                startTotalTimer()
                startContentTimer()
            else
                setResume()
                if checkIfEventConfigured(m.eventConfig.EVENTPLAY)
                    wsSend(getMessageTemplate(m.eventConfig.EVENTPLAY))
                end if
                startupDuration = m.startupTimer.TotalMilliseconds()
            end if

            if checkIfEventConfigured(m.eventConfig.EVENTPLAYING)
                wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYING))
            end if
            m.prevState = m.player.state
        else if state = "paused"
            m.videoPaused = true
            stopTotalTimer()
            stopContentTimer()

            m.pausePoint = m.player.position
            m.TSPAUSED = getCurrentTimestampInMillis()
            m.pauseTimer = CreateObject("roTimespan")
            m.pauseTimer.Mark()

            if checkIfEventConfigured(m.eventConfig.EVENTPAUSE)
                wsSend(getMessageTemplate(m.eventConfig.EVENTPAUSE))
            end if
        else if state = "buffering"
            stopTotalTimer()
            stopContentTimer()
            m.bufferStarted = true
            m.bufferStartTime = getCurrentTimestampInMillis()

            if m.videoType = "ad"
                m.adBufferStartTime = getCurrentTimestampInMillis()
            end if
            m.bufferEnd = false

'        if m.player.position > 1
            m.TSSTALLSTART = getCurrentTimestampInMillis()
'        end if

            if checkIfEventConfigured(m.eventConfig.EVENTBUFFERSTART)
                wsSend(getMessageTemplate(m.eventConfig.EVENTBUFFERSTART))
            end if

            if checkIfEventConfigured(m.eventConfig.EVENTBUFFERING)
                wsSend(getMessageTemplate(m.eventConfig.EVENTBUFFERING))
            end if

            if checkIfEventConfigured(m.eventConfig.EVENTSTALLSTART)
                wsSend(getMessageTemplate(m.eventConfig.EVENTSTALLSTART))
            end if
            m.prevState = m.player.state
        else if m.player.state = "error"
            m.numberOfErrors = m.numberOfErrors + 1
            if m.videoType = "content"
                m.numberOfErrorsContent = m.numberOfErrorsContent + 1
            end if
            if checkIfEventConfigured(m.eventConfig.EVENTERROR)
                m.template.event.attributes[m.eventConfig.METAERRORCODE] = m.player.errorCode.ToStr()
                m.template.event.attributes[m.eventConfig.METAERRORMSG] = m.player.errorMsg
                wsSend(getMessageTemplate(m.eventConfig.EVENTERROR))
                m.template.event.attributes = {}
            end if
            m.template.event.attributes = {}

            m.prevState = m.player.state
        else if m.player.state = "stopped"
            if checkIfEventConfigured(m.eventConfig.EVENTSTOP)
                wsSend(getMessageTemplate(m.eventConfig.EVENTSTOP))
            end if

            if m.player.content <> invalid
                if m.player.content.url <> invalid
                    m.contentUrlStop = m.player.content.url
                end if
            end if

            if m.streamType = "Live"
                stopTotalTimer()
                stopContentTimer()

                m.playRequestTriggered = true
                m.firstStart = true
            end if

            if m.streamType = "VOD"
                stopTotalTimer()
                stopContentTimer()
                m.firstFrameTriggered = true
                m.playRequestTriggered = true
                m.firstStart = true

                if checkIfEventConfigured(m.eventConfig.EVENTPLAYBACKCOMPLETE) and m.playbackCompleteTriggered = false and (m.player.position = m.player.duration)
                    wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYBACKCOMPLETE))
                    m.playbackCompleteTriggered = true
                    m.playbackTimer = invalid

                    if m.videoType = "content"
                        m.firstFrameTriggered = false
                        m.playRequestTriggered = false
                        m.firstStart = false
                        m.heartbeatTimer = invalid
                        m.playbackTimer = invalid

                        stopTotalTimer()
                        stopContentTimer()
                        m.playbackDurationTotal = 0
                        m.playbackDurationContent = 0
                    end if

                    m.firstFrameTriggered = false
                    m.playRequestTriggered = false
                    m.firstStart = false
                    m.heartbeatTimer = invalid
                    m.playbackTimer = invalid
                    stopTotalTimer()
                    stopContentTimer()
                    m.playbackDurationTotal = 0
                    m.playbackDurationContent = 0
                    m.TSLASTADHEARTBEAT = 0
                end if

                m.firstFrameTriggered = false
            end if
            m.prevState = m.player.state
        else if state = "finished"
            if checkIfEventConfigured(m.eventConfig.EVENTPLAYBACKCOMPLETE) and m.playbackCompleteTriggered = false
                wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYBACKCOMPLETE))
                m.playbackCompleteTriggered = true
                m.playbackTimer = invalid

                if m.videoType = "content"
                    m.firstFrameTriggered = false
                    m.playRequestTriggered = false
                    m.firstStart = false
                    m.heartbeatTimer = invalid
                    m.playbackTimer = invalid

                    stopTotalTimer()
                    stopContentTimer()
                    m.playbackDurationTotal = 0
                    m.playbackDurationContent = 0
                end if

                m.firstFrameTriggered = false
                m.playRequestTriggered = false
                m.firstStart = false
                m.heartbeatTimer = invalid
                m.playbackTimer = invalid
                stopTotalTimer()
                stopContentTimer()
                m.playbackDurationTotal = 0
                m.playbackDurationContent = 0
                m.TSLASTADHEARTBEAT = 0
            end if

            m.firstFrameTriggered = false
            m.prevState = m.player.state
        end if
    end if
end sub

' Runs at specific interval to check is quartile position is reached
sub playbackTimerContentFunction()
    if m.player <> invalid
        if m.playbackDurationContentTimer = invalid
            m.playbackDurationContentTimer = CreateObject("roTimespan")
        else
            m.playbackDurationContent = m.playbackDurationContent + m.playbackDurationContentTimer.TotalMilliseconds() ' m.playbackDurationContent + (m.adPlaybackDuration * 1000)
            m.playbackDurationContentTimer.Mark()
        end if
    end if
end sub

' Runs at specific interval to update the data
sub dataTimerFunction()
    videoRunning = false
    if m.player <> invalid
        ' is video running?
        if m.player.state = "playing"
            videoRunning = true
        end if
        ' is video Muted
        if videoRunning
            if m.player.mute <> invalid
                videoMute = m.player.mute
            end if

            if m.mute <> videoMute
                m.mute = videoMute
                if m.mute
                    if checkIfEventConfigured(m.eventConfig.EVENTMUTE)
                        playerMetrics()
                        wsSend(getMessageTemplate(m.eventConfig.EVENTMUTE))
                    end if
                else
                    if checkIfEventConfigured(m.eventConfig.EVENTUNMUTE)
                        playerMetrics()
                        wsSend(getMessageTemplate(m.eventConfig.EVENTUNMUTE))
                    end if
                end if
            end if

            ' Collect stream metadata and info

            ' timedMetadata
            m.player.timedMetaDataSelectionKeys = ["*"]
        end if
    end if
end sub

' Checks when player is ready to play
function playStateTimerFunction()
    if m.player <> invalid
        if m.playbackDurationTotalTimer = invalid
            m.playbackDurationTotalTimer = CreateObject("roTimespan")
        else
            m.playbackDurationTotal = m.playbackDurationTotal + m.playbackDurationTotalTimer.TotalMilliseconds()
            m.playbackDurationTotalTimer.Mark()
        end if
        if m.contentsQV <> invalid and m.contentsQV.Count() > 0
            if m.playbackDurationContent / 1000 >= m.contentsQV[0]
                m.template.event.attributes["qualified_view_sec"] = m.contentsQV[0]
                wsSend(getMessageTemplate(m.eventConfig.EVENTQVIEW))
                m.template.event.attributes = {}
                if m.contentsQV.count() > 1
                    m.contentsQV.Shift()
                else
                    m.contentsQV.Clear()
                end if
            end if
        end if

        if m.playbackDurationTotal >= m.top.events.interval * m.heartCount
            fluxMetricsData()
            if checkIfEventConfigured(m.eventConfig.EVENTHEARTBEAT)
                m.template.event.attributes["heartbeat_count"] = m.heartCount
                playerMetrics()
                wsSend(getMessageTemplate("heartbeat"))
                m.template.event.attributes = {}
                if m.heartbeatTimer = invalid
                    m.heartbeatTimer = CreateObject("roTimespan")
                    m.timeSinceLastHeartbeat = 0
                else
                    m.heartbeatTimer.Mark()
                end if
            end if

            m.heartCount = m.heartCount + 1
        end if
        if m.player.position <> invalid and m.player.duration <> invalid and m.streamType <> "Live"
            playerPosition = m.player.position
            playerDuration = m.player.duration
            m.template.event.attributes = {}
            if playerDuration > 0 and playerPosition > 0 and m.contentsMilestones.Count() > 0
                if playerPosition >= (playerDuration * (m.contentsMilestones[0] / 100))
                    if checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
                        if checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
                            m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = m.contentsMilestones[0] / 100
                        end if
                        m.milestonePercent = m.contentsMilestones[0] / 100
                        playerMetrics()
                        wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
                        m.TSLASTMILESTONE = getCurrentTimestampInMillis()
                        m.template.event.attributes = {}
                        if m.contentsMilestones.count() > 1
                            m.contentsMilestones.Shift()
                        else
                            m.contentsMilestones.Clear()
                        end if
                    end if
                end if
            end if
        end if
    end if
' end if
    if m.player.downloadedSegment <> invalid
        if m.player.downloadedSegment.segStreamBandwidth <> invalid
            if m.player.downloadedSegment.height <> 0
                m.streamB = m.player.downloadedSegment.segStreamBandwidth
                m.renditionBitrate = m.streamB
                if m.streamA <> m.streamB
                    playerMetrics()
                    if m.streamB < m.renditionVideoBitrate
                        m.template.event.attributes[m.eventConfig.METAABSSHIFT] = "down"
                    end if
                    if m.streamB > m.renditionVideoBitrate
                        m.template.event.attributes[m.eventConfig.METAABSSHIFT] = "up"
                    end if
                    m.streamA = m.streamB
                    m.renditionVideoBitrate = m.streamA
                    if checkIfEventConfigured(m.eventConfig.EVENTBITRATECHANGE)
                        playerMetrics()
                        wsSend(getMessageTemplate(m.eventConfig.EVENTBITRATECHANGE))
                    end if
                    if checkIfEventConfigured(m.eventConfig.EVENTRENDITIONCHANGE)
                        playerMetrics()
                        wsSend(getMessageTemplate(m.eventConfig.EVENTRENDITIONCHANGE))
                        m.TSRENDITIONCHANGE = getCurrentTimestampInMillis()
                        m.template.event.attributes = {}
                    end if
                end if
            end if
        end if
    end if

    if m.player.streamInfo <> invalid
        if m.player.streamInfo.streamBitrate <> invalid or m.player.streamInfo.streamBitrate <> 0
            m.streamBitrate = m.player.streamInfo.streamBitrate
            m.networkBandwidth = m.player.streamInfo.measuredBitrate / 1000
        end if
    end if
end function

' ----------------------- Collector Starts --------------------------------

' Method to get the message templat
function getMessageTemplate(event as String) as Object
'    m.template.user_details.app_session_id = getAppSessionID()
    m.eventCount = m.eventCount + 1
    if m.eventCount > 9999
        m.tenths = m.tenths + 1
        eventCount = 1
    end if
    evntCount$ = m.eventCount.ToStr()
    if Len(evntCount$) < 4
        eventCount$ = String((4 - Len(evntCount$)), "0") + evntCount$
    end if
    if m.tenths < 10
        tenth$ = "0" + m.tenths.toStr()
    else
        tenth$ = m.tenths.toStr()
    end if
    if m.debug = true
        ? "DZ-Print : "; m.template.user_details.app_session_id
        ? "DZ-Print : "; tenth$
        ? "DZ-Print : "; eventCount$
        ? "DZ-Print : "; m.eventCount
    end if

    m.template.event.metrics[m.eventConfig.METAEVENTCOUNT] = m.eventCount
    if m.template.user_details.app_session_id <> invalid
        m.template.event_id = m.template.user_details.app_session_id + "_" + tenth$ + "." + eventCount$
    else
        m.template.event_id = m.template.user_details.app_session_id + "_" + tenth$ + "." + eventCount$
    end if
    m.template.custom.Append(m.customSessionMeta)
    m.template.custom.Append(m.customPlayerMeta)

    if checkIfMetaConfigured(m.eventConfig.METASESSIONVIEWID)
        m.template.user_details.app_session_id = getAppSessionID()
    end if
    m.template.customer_code = m.responseBody.customer_code
    m.template.connector_list = m.responseBody.connector_list
    m.template.configuration_id = m.responseBody.configuration_id
    if checkIfMetaConfigured(m.eventConfig.METAUSERAGENT)
        m.template.user_details["user_agent"] = m.Device.getModelDisplayName() + "-" + m.Device.getModel() + "-" + getOSVersion() + "-" + m.Device.getChannelClientId()
    end if
    if checkIfMetaConfigured(m.eventConfig.METADZSDKVERSION)
        m.template.page["dz_sdk_version"] = "v2.2.2a"
    end if
    if checkIfMetaConfigured(m.eventConfig.METASITEDOMAIN)
        m.template.page["site_domain"] = m.appInfo.getTitle()
    end if
    m.template.event.type = event
    m.template.event.timestamp = getCurrentTimestampInMillis()
'     m.template.ops_metadata["client_ts_millis"] = m.template.event.timestamp
    m.template.Device.id = getUniqueDeviceId()

    m.template.user_details.is_anonymous = false
    m.template.player["player_name"] = "Roku Native Player"
    if checkIfMetaConfigured(m.eventConfig.METAPLAYERVERSION)
        m.template.player["player_version"] = getOSVersion()
    end if
    if checkIfMetaConfigured(m.eventConfig.METACONNECTIONTYPE)
        m.template.network["connectionType"] = getConnectionType()
    end if
    getMetaData()
    getGeoMeta()
    return m.template
end function

function getGeoMeta()
    if checkIfMetaConfigured(m.eventConfig.METAIP)
        m.template.user_details[m.eventConfig.METAIP] = getIpAddress()
    end if
    if checkIfMetaConfigured(m.eventConfig.METACITY)
        m.template.geo_location[m.eventConfig.METACITY] = getCity()
    end if

    if checkIfMetaConfigured(m.eventConfig.METALONGITUDE)
        m.template.geo_location[m.eventConfig.METALONGITUDE] = getLongitude()
    end if
    if checkIfMetaConfigured(m.eventConfig.METALATITUDE)
        m.template.geo_location[m.eventConfig.METALATITUDE] = getLatitude()
    end if
    if checkIfMetaConfigured(m.eventConfig.METAZIP)
        m.template.geo_location[m.eventConfig.METAZIP] = getZip()
    end if
    if checkIfMetaConfigured(m.eventConfig.METACOUNTRYCODE)
        m.template.geo_location[m.eventConfig.METACOUNTRYCODE] = getCountryCode()
    end if
    if checkIfMetaConfigured(m.eventConfig.METACOUNTRY)
        m.template.geo_location[m.eventConfig.METACOUNTRY] = getCountry()
    end if
    if checkIfMetaConfigured(m.eventConfig.METAREGIONCODE)
        m.template.geo_location[m.eventConfig.METAREGIONCODE] = getRegionCode()
    end if
    if checkIfMetaConfigured(m.eventConfig.METAREGION)
        m.template.geo_location[m.eventConfig.METAREGION] = getRegion()
    end if
    if checkIfMetaConfigured(m.eventConfig.METAOS)
        m.template.Device[m.eventConfig.METAOS] = getOS()
    end if
    if checkIfMetaConfigured(m.eventConfig.METADEVICETYPE)
        m.template.Device[m.eventConfig.METADEVICETYPE] = GetDeviceType()
    end if
    if checkIfMetaConfigured(m.eventConfig.METADEVICEID)
        m.template.Device[m.eventConfig.METADEVICEID] = getUniqueDeviceId()
    end if
    if checkIfMetaConfigured(m.eventConfig.METAADVERTISINGID)
        adId = getAdId()
        m.template.Device[m.eventConfig.METAADVERTISINGID] = getAdId()
    end if
    if checkIfMetaConfigured(m.eventConfig.METADEVICENAME)
        m.template.Device[m.eventConfig.METADEVICENAME] = getModelDisplayName()
    end if
    if checkIfMetaConfigured(m.eventConfig.METADEVICEMFG)
        m.template.Device[m.eventConfig.METADEVICEMFG] = "Roku, Inc."
    end if
    if checkIfMetaConfigured(m.eventConfig.METAVIDEOTYPE)
        m.template.video[m.eventConfig.METAVIDEOTYPE] = m.videoType
    end if
    if checkIfMetaConfigured(m.eventConfig.METAOSVERSION)
        m.template.Device[m.eventConfig.METAOSVERSION] = getOSVersion()
    end if
    if checkIfMetaConfigured(m.eventConfig.METAASN)
        m.template.network[m.eventConfig.METAASN] = getasn()
    end if
    if checkIfMetaConfigured(m.eventConfig.METAASNORG)
        m.template.network[m.eventConfig.METAASNORG] = getasnOrg()
    end if
    if checkIfMetaConfigured(m.eventConfig.METAISP)
        m.template.network[m.eventConfig.METAISP] = getISP()
    end if
    if checkIfMetaConfigured(m.eventConfig.METATIMEZONE)
        m.template.geo_location[m.eventConfig.METATIMEZONE] = getTimezone()
    end if
    if checkIfMetaConfigured(m.eventConfig.METATIMEZONEOFFSET)
        m.template.geo_location[m.eventConfig.METATIMEZONEOFFSET] = getTimezoneOffset() / 3600
    end if
    if checkIfMetaConfigured(m.eventConfig.METACONTINENT)
        m.template.geo_location[m.eventConfig.METACONTINENT] = getContinent()
    end if
    if checkIfMetaConfigured(m.eventConfig.METACONTINENTCODE)
        m.template.geo_location[m.eventConfig.METACONTINENTCODE] = getContinentCode()
    end if
    if checkIfMetaConfigured(m.eventConfig.METADISTRICT)
        m.template.geo_location[m.eventConfig.METADISTRICT] = getDistrict()
    end if
end function

' Function to get the metadata
function getMetaData()
    if m.player <> invalid and m.player.content <> invalid
        if checkIfMetaConfigured(m.eventConfig.METADURATION)
            playerDuration = 0
            if m.player <> invalid
                if m.player.duration <> invalid
                    playerDuration = m.player.duration
                end if
            end if
            m.template.video[m.eventConfig.METADURATION] = playerDuration
        end if

        if checkIfMetaConfigured(m.eventConfig.METAIP)
            m.template.user_details[m.eventConfig.METAIP] = getIpAddress()
        end if
        if checkIfMetaConfigured(m.eventConfig.METACITY)
            m.template.geo_location[m.eventConfig.METACITY] = getCity()
        end if
        if checkIfMetaConfigured(m.eventConfig.METAZIP)
            m.template.geo_location[m.eventConfig.METAZIP] = getZip()
        end if
        if checkIfMetaConfigured(m.eventConfig.METACOUNTRYCODE)
            m.template.geo_location[m.eventConfig.METACOUNTRYCODE] = getCountryCode()
        end if
        if checkIfMetaConfigured(m.eventConfig.METACOUNTRY)
            m.template.geo_location[m.eventConfig.METACOUNTRY] = getCountry()
        end if
        if checkIfMetaConfigured(m.eventConfig.METAREGIONCODE)
            m.template.geo_location[m.eventConfig.METAREGIONCODE] = getRegionCode()
        end if
        if checkIfMetaConfigured(m.eventConfig.METAREGION)
            m.template.geo_location[m.eventConfig.METAREGION] = getRegion()
        end if
        if checkIfMetaConfigured(m.eventConfig.METATIMEZONE)
            m.template.geo_location[m.eventConfig.METATIMEZONE] = getTimezone()
        end if
        if checkIfMetaConfigured(m.eventConfig.METATIMEZONEOFFSET)
            m.template.geo_location[m.eventConfig.METATIMEZONEOFFSET] = getTimezoneOffset()
        end if
        if checkIfMetaConfigured(m.eventConfig.METACONTINENT)
            m.template.geo_location[m.eventConfig.METACONTINENT] = getContinent()
        end if
        if checkIfMetaConfigured(m.eventConfig.METACONTINENTCODE)
            m.template.geo_location[m.eventConfig.METACONTINENTCODE] = getContinentCode()
        end if
        if checkIfMetaConfigured(m.eventConfig.METADISTRICT)
            m.template.geo_location[m.eventConfig.METADISTRICT] = getDistrict()
        end if
        if checkIfMetaConfigured(m.eventConfig.METAOS)
            m.template.Device[m.eventConfig.METAOS] = getOS()
        end if
        if checkIfMetaConfigured(m.eventConfig.METADEVICETYPE)
            m.template.Device[m.eventConfig.METADEVICETYPE] = GetDeviceType()
        end if
        if checkIfMetaConfigured(m.eventConfig.METADEVICEID)
            m.template.Device[m.eventConfig.METADEVICEID] = getUniqueDeviceId()
        end if
        if checkIfMetaConfigured(m.eventConfig.METAADVERTISINGID)
            adId = getAdId()
            m.template.Device[m.eventConfig.METAADVERTISINGID] = getAdId()
        end if
        if checkIfMetaConfigured(m.eventConfig.METADEVICENAME)
            m.template.Device[m.eventConfig.METADEVICENAME] = getModelDisplayName()
        end if
        if checkIfMetaConfigured(m.eventConfig.METADEVICEMFG)
            m.template.Device[m.eventConfig.METADEVICEMFG] = "Roku, Inc."
        end if
        if checkIfMetaConfigured(m.eventConfig.METAVIDEOTYPE)
            m.template.video[m.eventConfig.METAVIDEOTYPE] = m.videoType
        end if
        if checkIfMetaConfigured(m.eventConfig.METAOSVERSION)
            m.template.Device[m.eventConfig.METAOSVERSION] = getOSVersion()
        end if
        if checkIfMetaConfigured(m.eventConfig.METAASN)
            m.template.network[m.eventConfig.METAASN] = getasn()
        end if
        if checkIfMetaConfigured(m.eventConfig.METAASNORG)
            m.template.network[m.eventConfig.METAASNORG] = getasnOrg()
        end if
        if checkIfMetaConfigured(m.eventConfig.METAISP)
            m.template.network[m.eventConfig.METAISP] = getISP()
        end if

        if checkIfMetaConfigured(m.eventConfig.METASUBTITLES)
            if m.player.globalCaptionMode <> invalid
                if m.player.globalCaptionMode = "Off"
                    m.template.player[m.eventConfig.METASUBTITLES] = false
                else if m.player.globalCaptionMode = "On"
                    m.template.player[m.eventConfig.METASUBTITLES] = true
                else
                    m.template.player[m.eventConfig.METASUBTITLES] = false
                end if
            else
                m.template.player[m.eventConfig.METASUBTITLES] = false
            end if
        end if

        if checkIfMetaConfigured(m.eventConfig.METACONTROLS)
            metacontrol = false
            if m.player <> invalid
                if m.player.control <> invalid
                    if m.player.control <> ""
                        metacontrol = true
                    end if
                end if
            end if
            m.template.player[m.eventConfig.METACONTROLS] = metacontrol
        end if
        if checkIfMetaConfigured(m.eventConfig.METALOOP)
            metaloop = false
            if m.player <> invalid
                if m.player.loop <> invalid
                    if m.player.loop
                        metaloop = true
                    end if
                end if
            end if
            m.template.player[m.eventConfig.METALOOP] = metaloop
        end if
        if checkIfMetaConfigured(m.eventConfig.METAREADYSTATE)
            m.template.player[m.eventConfig.METAREADYSTATE] = getPlayerReadyState()
        end if
        if checkIfMetaConfigured(m.eventConfig.METASESSIONVIEWID)
            m.template.user_details.app_session_id = getAppSessionID()
        end if

        if checkIfMetaConfigured(m.eventConfig.METASESSIONSTARTTIMESTAMP)
            m.template.user_details[m.eventConfig.METASESSIONSTARTTIMESTAMP] = getSessionStartTimestamp()
        end if

        if checkIfMetaConfigured(m.eventConfig.METALONGITUDE)
            m.template.geo_location[m.eventConfig.METALONGITUDE] = getLongitude()
        end if
        if checkIfMetaConfigured(m.eventConfig.METALATITUDE)
            m.template.geo_location[m.eventConfig.METALATITUDE] = getLatitude()
        end if
        if checkIfMetaConfigured(m.eventConfig.METADESCRIPTION)
            metadescription = ""
            if m.player <> invalid
                if m.player.content <> invalid
                    if m.player.content.description <> invalid
                        metadescription = m.player.content.description
                    end if
                end if
            end if
            m.template.video[m.eventConfig.METADESCRIPTION] = metadescription
        end if
        if checkIfMetaConfigured(m.eventConfig.METATITLE)
            metatitle = ""
            if m.videoType = "ad" and m.adTitle <> invalid
                metatitle = m.adTitle
            end if
            if m.player <> invalid and m.videoType = "content"
                if m.player.content <> invalid
                    if m.player.content.title <> invalid
                        metatitle = m.player.content.title
                    end if

                    if m.player.content.url <> invalid
                        m.metaurl = m.player.content.url
                    end if
                end if
            end if
            m.template.video[m.eventConfig.METATITLE] = metatitle
        end if
        if checkIfMetaConfigured(m.eventConfig.METASOURCE)
            metatitle = ""
            if m.videoType = "ad" and m.adUrl <> invalid
                m.metaurl = m.adUrl
            end if
            if m.player <> invalid and m.videoType = "content"
                if m.player.content <> invalid
                    if m.player.content.url <> invalid
                        m.metaurl = m.player.content.url
                    end if
                end if
            end if
            m.template.video[m.eventConfig.METASOURCE] = m.metaurl
        end if
        if checkIfMetaConfigured(m.eventConfig.METASTREAMINGPROTOCOL)
            if m.player.videoFormat <> invalid
                m.template.player[m.eventConfig.METASTREAMINGPROTOCOL] = m.player.videoFormat
            end if
        end if
        if m.player.content <> invalid
            if m.player.content.Live <> invalid
                if m.player.content.Live = true
                    m.streamType = "Live"
                else
                    if m.template.custom["channelType"] <> ""
                        if m.template.custom["channelType"] = "linear"
                            m.streamType = "Live"
                        else
                            m.streamType = "VOD"
                        end if
                    else
                        if m.player.content.Title = "Live Stream" and m.videoType <> "ad"
                            m.streamType = "Live"
                        else
                            m.streamType = ""
                        end if
                    end if
                end if
            end if
        end if

        if checkIfMetaConfigured(m.eventConfig.METASTREAMINGTYPE)
            m.template.player[m.eventConfig.METASTREAMINGTYPE] = m.streamType
        end if

        if checkIfMetaConfigured(m.eventConfig.METAISMUTED)
            m.template.player[m.eventConfig.METAISMUTED] = m.mute
        end if
        if checkIfMetaConfigured(m.eventConfig.METADEFAULTMUTED)
            m.template.player[m.eventConfig.METADEFAULTMUTED] = m.defaultMute
        end if
        if checkIfMetaConfigured(m.eventConfig.METADEFAULTPLAYBACKRATE)
            m.template.player[m.eventConfig.METADEFAULTPLAYBACKRATE] = m.defaultRate
        end if
        m.displaySize = m.Device.getDisplaySize()
        m.playerHeight = m.displaySize.h
        m.playerWidth = m.displaySize.w
        if checkIfMetaConfigured(m.eventConfig.METAPLAYERHEIGHT)
            m.template.video[m.eventConfig.METAPLAYERHEIGHT] = m.playerHeight
        end if
        if checkIfMetaConfigured(m.eventConfig.METAPLAYERWIDTH)
            m.template.video[m.eventConfig.METAPLAYERWIDTH] = m.playerWidth
        end if
        if checkIfMetaConfigured(m.eventConfig.METATITLE)
            metatitle = ""
            if m.videoType = "ad" and m.adTitle <> invalid
                metatitle = m.adTitle
            end if
            if m.player <> invalid
                if m.player.content.title <> invalid and m.videoType = "content"
                    metatitle = m.player.content.title
                end if
                if m.player.content.url <> invalid and m.videoType = "content"
                    m.metaurl = m.player.content.url
                end if
            end if
            m.template.video[m.eventConfig.METATITLE] = metatitle
        end if

        if checkIfMetaConfigured(m.eventConfig.METAFRAMERATE)
            if m.player.FrameRate <> invalid
                m.template.video[m.eventConfig.METAFRAMERATE] = m.player.FrameRate
            else
                m.template.video[m.eventConfig.METAFRAMERATE] = 30
            end if
        end if
    end if
end function

' Function to get video URL/set url in base
function getContentUrl()
    if m.player <> invalid
        if m.player.content <> invalid
            if m.player.content.Url <> invalid
                setContentUrlToBase(m.player.content.Url)
'                 If checkIfMetaConfigured(m.eventConfig.METAASSETID)
'                 m.template.video[m.eventConfig.METAASSETID] = m.device.GetRandomUUID()
'                 end if
                return m.player.content.Url
            else
                return ""
            end if
        else
            return ""
        end if
    end if
end function

function onKeyEvent(key as String, press as Boolean) as Boolean
    ? "Player: keyevent = "; key
    if press and key = "play" then
        return true
    end if
    if press and key = "back" then
        ' handle Back button, by exiting play
        return true
    end if
    return false
end function

function startTotalTimer()
    if m.playbackDurationTotalTimer = invalid
        m.playbackDurationTotalTimer = CreateObject("roTimespan")
    end if
    m.playStateTimer.control = "start"
end function

function startContentTimer()
    if m.playbackDurationContentTimer = invalid
        m.playbackDurationContentTimer = CreateObject("roTimespan")
    end if
    m.playbackTimerContent.control = "start"
end function
function stopTotalTimer()
    m.playStateTimer.control = "stop"
    if m.playbackDurationTotalTimer <> invalid
        m.playbackDurationTotal = m.playbackDurationTotal + m.playbackDurationTotalTimer.TotalMilliseconds()
    end if
    m.playbackDurationTotalTimer = invalid
end function

function stopContentTimer()
    m.playbackTimerContent.control = "stop"
    if m.playbackDurationContentTimer <> invalid
        m.playbackDurationContent = m.playbackDurationContent + m.playbackDurationContentTimer.TotalMilliseconds()
    end if
    m.playbackDurationContentTimer = invalid
end function

     