function setLogLevel(level)
    if m.logLevels.doesExist(LCase(level))
        m.DzLogLevelInt = m.logLevels.Lookup(LCase(level))
        ? "DZ-Log: Data Zoom Console Logging Enabled with Level "; m.DzLogLevelInt ;" - "; level
    else
        ? "DZ-Log: DATAZOOM LOGGING NOT CHANGED"
        ? "DZ-Log: PLEASE USE STRING VALUES TO CHANGE DATAZOOM LOGGING LEVEL"
        ? "DZ-Log: Accepted string values are : off, error, warning, info, debug, verbose"
    end if
end function


function logError(logMessage)
    if m.logLevels.error <= m.DzLogLevelInt 
       ? "DZ-Log (error) - "; logMessage
    end if
end function

function logWarning(logMessage)
    if m.logLevels.warning <= m.DzLogLevelInt
       ? "DZ-Log (warning) - "; logMessage
    end if
end function

function logInfo(logMessage)
    if m.logLevels.info <= m.DzLogLevelInt
       ? "DZ-Log (info) - "; logMessage
    end if
end function

function logDebug(logMessage)
    if m.logLevels.debug <= m.DzLogLevelInt
       ? "DZ-Log (debug) - "; logMessage
    end if
end function

function logVerbose(logMessage)
    if m.logLevels.verbose <= m.DzLogLevelInt
    ? "DZ-Log (verbose) - "; logMessage
    end if
end function
