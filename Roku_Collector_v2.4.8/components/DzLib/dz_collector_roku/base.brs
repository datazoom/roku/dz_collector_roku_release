sub Init()
    m.Device = CreateObject("roDeviceInfo")
    m.task = CreateObject("roSGNode", "Task_CallAPI")
    m.debug = false
    m.dzInitBool = false
    m.sendSamplingTask = CreateObject("roSGNode", "Task_SendSampling")
    m.geoTask = CreateObject("roSGNode", "Task_GeoData")
    m.serverTimeTask = CreateObject("roSGNode", "Task_GetServerTime")
    m.date = CreateObject("roDateTime")
    m.connectionTimer = m.top.findNode("connectionTimer")
    m.connectionTimer.control = "start"
    m.apiBusTimer = m.top.findNode("apiBusTimer")
    m.apiBusTimer.control = "start"
    m.apiBusTimer.ObserveField("fire", "apiBus")
    m.timerCount = 0
    m.setViewIDFlag = true
    m.appInfo = CreateObject("roAppInfo")
    m.videoCntKeyFlag = false
    m.GeoData = {
        city: "",
        country: "",
        countryCode: "",
        lat: "",
        lon: "",
        regionCode: "",
        asn: "",
        asnOrganization: "",
        client_ip: "",
        isp: "",
        region: "",
        zip: "",
        timezone: "",
        timezone_offset: ""
    }
    m.dzBaseRegister = CreateObject("roRegistrySection", "sessionIdReg")
    m.playerReadyState = 0
    m.noOfVid = 1
    m.videoURL = ""
    m.vSessionID = {}
    uniqueName = m.appInfo.GetID() + "-" + playerType()
    m.vewSessionIdKey = uniqueName + "-sessionViewId"
    m.noOfVideosKeyName = uniqueName + "-noOfVideos"
    m.sessionIdKey = uniqueName + "-sessionId"
    m.sessionIdTimeKey = uniqueName + "-sessionIdTime"
    m.sendTimeStamp = getCurrentTimestampInMillis()
    m.longPauseFlag = false
    m.retrieveTimeStamp = 0
    m.eventConfirmed = true
    m.apiReceivedTime = 0
    m.syncApiDelay = 0
    m.clientTimeOffset = 0
    m.clientTime = 0
    m.apiTimer = CreateObject("roTimespan")
    m.syncTimer = CreateObject("roTimespan")
    m.brokerDelay = 0
    m.pausedTime = 0
    m.viewIDSet = false
    m.dzBaseRegister.Delete(m.sessionIdKey)
    m.dzBaseRegister.Delete(m.vewSessionIdKey)
    m.dzBaseRegister.Flush()
    m.serverTimeMillis = 0
    m.media_type = "content"
    m.sessionStartTimestamp = 0
    m.dataBuffer = CreateObject("roList")
    m.brokerResponseFired = false
    DIM wsDatTmp[10]
    m.wsDataTmp = wsDatTmp
    DIM evntJson[10]
    m.eventJson = evntJson
    m.delayEventTimer = CreateObject("roTimespan")
    m.storedData = CreateObject("roArray", 5, true)
    m.geoCheck = ["client_ip",
        "country",
        "country_code",
        "region_code",
        "region",
        "city",
        "district",
        "postal_code",
        "latitude",
        "longitude",
        "timezone_name",
        "timezone_offset",
        "isp",
        "asn_org",
        "asn",
        "mobile_connection",
        "continent",
        "continent_code",
    "district"]
    m.getGeo = false
    m.serverTimeUrl = "https://broker.datazoom.io/broker/v1/getEpochMillis"
    m.brokerUrl = "https://broker.datazoom.io/broker/v1/logs"
    m.pendingForSampling = true
    m.samplingURL = ""
    m.samplingOverride = false
    m.samplingTemplate = {}
    m.samplingTemplate.sampling_definition_params = {}
    m.samplingTemplate.sampling_criteria_params = {}
    m.samplingTemplate.sampling_criteria_params.sampling = {}
    m.samplingTemplate.matching = {}
    m.samplingConfirmed = true
    m.ipApiUrl = "https://pro.ip-api.com/json/?key=StlpBuYEtQbYlZl&fields=37351423"
    m.datazoomLoaded = false
end sub

' Method to initiate the collector library.
sub initiateCollector()
    if m.top.initiateCollector
        ResetAll()
        getCustomerConfiguration()
        m.template.user_details.app_session_id = getAppSessionId()
        m.sessionStartTimestamp = getCurrentTimestampInMillis()
        m.eventConfig = getLibConfig().events
        if m.top.libConfiguration <> invalid
            if m.debug = true
                ? "DATAZOOM ROKU BASE(NATIVE) V3 SDK INITIALIZED"
            end if
            ' playerType() must be available in collector brs file
            uniqueName = (m.top.libConfiguration.configId) + m.appInfo.GetID() + "-" + playerType()
            m.noOfVideosKeyName = uniqueName + "-noOfVideos"
            m.vewSessionIdKey = uniqueName + "-sessionViewId"
            m.sessionIdKey = uniqueName + "-sessionId"
            m.sessionIdTimeKey = uniqueName + "-sessionIdTime"
            m.videoCntKeyFlag = true
        else
            if m.debug = true
                ? "FAILED TO INITIALISE DATAZOOM SDK WRONG OR NON EXISTING CONFIG ID"
            end if
        end if
    end if
end sub

' Method to initiate the player
sub initiatePlayer()
    if m.top.initiatePlayer
        if m.top.connectionSuccess = true
            if m.debug = true
                ? "DATAZOOM ROKU NATIVE V3 SDK PLAYER INITIALIZED"
            end if
            m.playerStates = m.top.playerInit.player
            m.playerStates.observeField("state", "playerStateBase")
            m.eventConfig = getLibConfig().events
            configureEvents()
        else
            ? "Datazoom SDK is not Fully Initialized"
        end if
    end if
end sub

' Method to get the customer configuration from beacon-service.
sub getCustomerConfiguration()
    m.template.event.metrics = {}
    m.apiTimer.Mark()
    configId = m.top.libConfiguration.configId
    configURL = m.top.libConfiguration.configURL
    requestData = {}
    requestData.httpMethodString = "GET"
    url = configURL.toStr() + "/beacon/v2/config?configuration_id=" + configId.toStr()
    requestData.urlString = url
    requestData.postBodyString = invalid
    requestData.headersAssociativeArray = invalid
    m.task.setField("requestData", requestData)
    m.task.observeField("result", "onResponseReceived")
    m.task.control = "RUN"
end sub

' Player operations
sub playerStateBase()
    if m.playerStates.state = "playing"
        m.playerReadyState = 1
    else if m.playerStates.state = "stopped"
        m.setViewIDFlag = true
    else if m.playerStates.state = "finished"
        m.setViewIDFlag = true
    else if m.playerStates.state = "error"
        m.setViewIDFlag = true
    end if
end sub

' Calls function setSessionViewId
function callToSetNoOfVideos()
    if m.setViewIDFlag
        m.setViewIDFlag = false
    end if
end function

' After API for customer configuration received.
sub onResponseReceived()
    if m.task.result.responsecode <> invalid
        m.configStatusCode = m.task.result.responsecode
    else
        m.configStatusCode = 505
    end if
    if m.configStatusCode = 200
        m.brokerDelay = m.apiTimer.TotalMilliseconds()
        ' Get collector config
        if m.task.result <> invalid and m.task.result.bodystring <> invalid
            m.responseBody = ParseJson(m.task.result.bodystring)
            if m.responseBody <> invalid
                m.top.configuration = m.responseBody
                if m.responseBody.endpoints <> invalid
                    ' -----------Data point validation----------------
                    m.timerCount = 0
                    dataPointValidation()
                    if checkSessionData() <> true
                        m.appSessionID = getAppSessionID()
                    end if
                    m.template.user_details["app_session_start_ts_ms"] = getCurrentTimestampInMillis()
                    ' ------------------------------------------------
                    if m.responseBody.endpoints <> invalid
                        if m.responseBody.endpoints.server_ts <> invalid
                            m.serverTimeUrl = m.responseBody.endpoints.server_ts
                            getServerTime(m.serverTimeUrl)
                        end if
                        if m.responseBody.endpoints.sampling <> invalid
                            m.samplingURL = m.responseBody.endpoints.sampling
                        end if
                        if m.responseBody.endpoints.event_ingest <> invalid
                            m.brokerUrl = m.responseBody.endpoints.event_ingest
                        end if
                    end if
                    
                       ' ------------- set Geo Loacation Call URL ----------------

                    if m.responseBody.endpoints.ip_api <> invalid or m.responseBody.endpoints.ip_api <> ""
                        m.ipApiUrl = m.responseBody.endpoints.ip_api
                        m.top.ipApiUrl = m.responseBody.endpoints.ip_api
                    else
                        m.top.ipApiUrl = "https://pro.ip-api.com/json/?key=StlpBuYEtQbYlZl&fields=37351423"
                        m.ipApiUrl = "https://pro.ip-api.com/json/?key=StlpBuYEtQbYlZl&fields=37351423"
                    end if
                    
                    ' -------------- check if geo IP lookup is needed -----------------
                    for each metaType in m.responseBody.datapoints.metadata
                        for each geoChk in m.geoCheck
                            if metaType = geoChk
                                m.getGeo = true
                                exit for
                            end if
                        end for
                        if m.getGeo = true
                            getGeoData()
                            exit for
                        end if
                    end for

                    if m.responseBody.datapoints <> invalid
                        if m.responseBody.datapoints.events <> invalid
                            m.top.events = m.responseBody.datapoints.events
                            m.top.fluxData = m.responseBody.datapoints.flux_data
                            m.top.metaData = m.responseBody.datapoints.metadata
                        end if
                    end if



                    ' ------------ set milestones ---------------
                    if m.responseBody.content_milestone_percent <> invalid
                        m.top.contentMilestones = m.responseBody.content_milestone_percent
                    end if

                    if m.responseBody.ad_milestone_percent <> invalid
                        m.top.adMilestones = m.responseBody.ad_milestone_percent
                    end if
                    ' -------------------------------------------
                    ' ------------ set qualified view -----------
                    if m.responseBody.content_qv_sec <> invalid
                        m.top.contentQV = m.responseBody.content_qv_sec
                    end if
                    if m.responseBody.ad_qv_sec <> invalid
                        m.top.adQV = m.responseBody.ad_qv_sec
                    end if

                    ' -------------------------------------------
                    ' ------------- set sampling ----------------
                    if m.responseBody.sampling <> invalid
                        if m.responseBody.sampling.enable <> invalid
                            if m.responseBody.sampling.is_sampling = "true"
                                m.sampleIn = true
                                m.pendingForSampling = false
                                m.samplingDefNameSpace = m.responseBody.sampling.definition_namespace
                                m.samplingDefKeyName = m.responseBody.sampling.definition_key_name
                                m.samplingKeyName = m.responseBody.sampling.criteria_sampling_key_name
                                m.samplingKeyValue = "TEST"
                                m.samplingTemplate.sampling_request_id = m.Device.getRandomUUID()
                                m.samplingTemplate.sampling_override = "NONE"
                                m.samplingTemplate.sampling_definition_params.namespace = m.samplingDefNameSpace
                                m.samplingTemplate.sampling_definition_params.key_name = m.samplingDefKeyName
                                m.samplingTemplate.sampling_definition_params.key_value = m.responseBody.configuration_id
                                m.samplingTemplate.sampling_criteria_params.sampling.key_name = m.samplingKeyName
                                m.samplingTemplate.sampling_criteria_params.sampling.key_value = m.appSessionId
                                sendToSampling(m.samplingTemplate)
                            else
                                m.sampleIn = false
                            end if
                        end if
                    end if
                    dzInitialized()
                    ' -----------------------------------------------------------------------------------------
                end if
            else
                if m.debug = true
                    ? "DataZoomSDK failed to initialize - Invalid Configuration"
                end if
                m.top.connectionSuccess = false
            end if
        else
            if m.debug = true
                ? "DataZoomSDK failed to initialize - Invalid Response"
            end if
            m.top.connectionSuccess = false
        end if
    else
        ? "DataZoomSDK failed to initialize - Service or config unavailable"
        m.top.connectionSuccess = false
    end if
end sub

function ClientSideSampling()
end function

function sendToSampling(samplingData)
    m.samplingDataTmp = samplingData
    buffered = false
    requestData = {}
    requestData.httpMethodString = "POST"
    requestData.urlString = m.samplingURL
    requestData.headersAssociativeArray = { "Content-Type": "application/json", "cache-control": "no-cache" }
    requestData.postBodyString = FormatJson(samplingData)

    if m.samplingConfirmed = true
        if m.storedSamplingData <> invalid
            if m.storedSamplingData.Count() > 0
                for each buff in m.storedSamplingData
                    if buff["sampling_request_id"] = wsData["sampling_request_id"]
                        buffered = true
                    end if
                end for

                if buffered = false
                    m.storedSamplingData.Push(samplingData)
                end if

                requestData.postBodyString = FormatJson(m.storedSamplingData)
                m.storedData = invalid
            else
                if isArray(samplingData)
                    requestData.postBodyString = FormatJson(samplingData)
                else
                    requestData.postBodyString = FormatJson([samplingData])
                end if
            end if
        end if
        m.sendSamplingTask.setField("requestData", requestData)
        m.sendSamplingTask.observeField("result", "onSamplingResponse")
        m.sendSamplingTask.control = "RUN"
        m.samplingResponseFired = false
        m.samplingConfirmed = false
    else
        storeSamplingData(samplingData, true)
    end if
end function

sub onSamplingResponse()
    if m.samplingResponseFired = false
        if m.sendSamplingTask.result <> invalid
            if m.sendSamplingTask.result.responsecode <> invalid
                m.samplingStatusCode = m.sendSamplingTask.result.responsecode
            else
                m.samplingStatusCode = 505
            end if
            if m.sendSamplingTask.result <> invalid
                if m.samplingStatusCode >= 500 and m.samplingStatusCode < 600 or m.samplingStatusCode = 429
                    m.samplingResponseFired = true
                    m.samplingConfirmed = false
                    storeSamplingData(m.samplingDataTmp, true)
                    m.samplingResponseFired = true
                    m.samplingConfirmed = true
                else if m.apiStatusCode = 400
                    m.samplingConfirmed = false
                    m.samplingResponseFired = true
                else
                    m.samplingConfirmed = true
                    m.samplingResponseFired = true
                    retrieveSamplingData()
                end if
            else
                m.samplingResponseFired = true
            end if
        else
            storeSamplingData(samplingData, true)
            m.sendSamplingTask.result = invalid
        end if
    end if
end sub

function retrieveSamplingData()
    sendCount = 0
    jsonArray = CreateObject("roArray", 8, true)
    if m.dzBaseRegister.Exists("samplingData")
        dataArray = {}
        if m.dzBaseRegister.Read("samplingData") <> ""
            dataArray = ParseJson(m.dzBaseRegister.Read("samplingData"))
            m.dzBaseRegister.Delete("samplingData")
        end if

        if dataArray.Count() > 0
            for each key in dataArray
                jsonArray.push(dataArray[key])
            end for
            m.storedSamplingData = jsonArray
        end if
    end if
end function

function storeSamplingData(samplingData, flag)
    inStore = false
    if isArray(samplingData)
    else
        if m.dzBaseRegister.Exists("samplingData")
            dataArray = {}
            dataArray = ParseJson(m.dzBaseRegister.Read("samplingData"))
            if dataArray = invalid
                dataArray = {}
            end if
            for each storedKey in dataArray
                if dataArray[storedKey]["sampling_request_id"] = samplingData["sampling_request_id"]
                    inStore = true
                end if
            end for
            if inStore = false
                if isArray(samplingData)
                    for each samDat in samplingData
                        key = getCurrentTimestampInMillis().toStr() + randDigit(3).toStr()
                        dataArray[key] = samDay
                    end for
                else
                    key = getCurrentTimestampInMillis().toStr() + randDigit(3).toStr()
                    dataArray[key] = samplingData
                end if
            end if

            m.dzBaseRegister.Write("samplingData", FormatJson(dataArray))
            if flag
            end if
        else
            dataArray = {}
            key = getCurrentTimestampInMillis().toStr() + randDigit(3).toStr()

            for each storedKey in dataArray
                if dataArray[storedKey] = samplingData
                    inStore = true
                end if
            end for

            if inStore = false
                dataArray[key] = samplingData
            end if
            m.dzBaseRegister.Write("samplingData", FormatJson(dataArray))
            if flag
'            print "Processed data in storage : Count = "  dataArray.Count()
            end if
        end if
    end if
end function

function getGeoData()
    requestData = {}
    requestData.httpMethodString = "GET"
    url = m.ipApiUrl
    requestData.urlString = url
    requestData.postBodyString = invalid
    requestData.headersAssociativeArray = invalid
    m.geoTask.setField("requestData", requestData)
    m.geoTask.observeField("result", "onGeoResponseReceived")
    m.geoTask.control = "RUN"
end function

function getServerTime(serverTimeUrl)
    m.clientTime = getCurrentTimestampInMillis()
    m.syncTimer.Mark()
    requestData = {}
    requestData.httpMethodString = "GET"
    if m.serverTimeUrl <> invalid
        url = m.serverTimeUrl
    else
        url = "https://broker.datazoom.io/broker/v1/getEpochMillis"
    end if
    requestData.urlString = url
    requestData.postBodyString = invalid
    requestData.headersAssociativeArray = invalid
    m.serverTimeTask.setField("requestData", requestData)
    m.serverTimeTask.observeField("result", "onServerTimeReceived")
    m.serverTimeTask.control = "RUN"
end function

' After API for customer configuration received.
sub onGeoResponseReceived()
    if m.geoTask.result.responsecode <> invalid
        m.geoResponseStatusCode = m.geoTask.result.responsecode
    else
        m.geoResponseStatusCode = 505
    end if
    if m.geoResponseStatusCode = 200
        if m.geoTask.result <> invalid and m.geoTask.result.bodystring <> invalid and m.geoTask.result.responsecode = 200
            m.geoResponseBody = ParseJson(m.geoTask.result.bodystring)
            if m.geoResponseBody <> invalid
                m.GeoData.city = m.geoResponseBody.city
                m.GeoData.countryCode = m.geoResponseBody.countryCode
                m.GeoData.country = m.geoResponseBody.country
                m.GeoData.lat = m.geoResponseBody.lat
                m.GeoData.lon = m.geoResponseBody.lon
                m.GeoData.regionCode = m.geoResponseBody.region
                m.GeoData.asn = m.geoResponseBody.as
                m.GeoData.asnOrganization = m.geoResponseBody.org
                m.GeoData.client_ip = m.geoResponseBody.query
                m.GeoData.isp = m.geoResponseBody.isp
                m.GeoData.region = m.geoResponseBody.regionName
                m.GeoData.zip = m.geoResponseBody.zip
                m.GeoData.timezone = m.geoResponseBody.timezone
                m.GeoData.timezone_offset = m.geoResponseBody.offset
                m.GeoData.continent = m.geoResponseBody.continent
                m.GeoData.continentCode = m.geoResponseBody.continentCode
                m.GeoData.district = m.geoResponseBody.district
                m.geoReceived = true
            end if
        else
            m.geoReceived = false
        end if
    else
        m.geoReceived = false
        ? "Geo location service is not available"
    end if
end sub

sub dzInitialized()
    m.top.connectionSuccess = true
'    if m.geoReceived = true
        if checkIfEventConfigured(m.eventConfig.EVENTDATAZOOMLOADED) and m.datazoomLoaded = false
            m.template.event.metrics = {}
            m.template.event.metrics[m.eventConfig.METAEVENTCOUNT] = 1
            wsSend(getMessageTemplate(m.eventConfig.EVENTDATAZOOMLOADED))
            m.TSLOADED = getCurrentTimestampInMillis()
            ? "Datazoom SDK Initialized"
            m.datazoomLoaded = true
        end if
'    else
'        ? "NO GEO DATA"
'    end if
end sub

sub onServerTimeReceived()
    m.syncApiDelay =  m.syncTimer.TotalMilliseconds()
    if m.serverTimeTask.result <> invalid and m.serverTimeTask.result.bodystring <> invalid and m.serverTimeTask.result.responsecode = 200
        m.serverTimeResponseBody = ParseJson(m.serverTimeTask.result.bodystring)
        if m.serverTimeResponseBody <> invalid
            m.serverTimeMillis = m.serverTimeResponseBody.epoch_millis
            m.timeDiff = m.serverTimeMillis - m.clientTime
        else
            m.serverTimeMillis = m.clientTime
            m.timeDiff = m.serverTimeMillis - m.clientTime
        end if
    else
        m.serverTimeMillis = 0
    end if
    if m.serverTimeMillis <> invalid
        if m.brokerDelay <> 0
            m.clientTimeOffset = m.serverTimeMillis - m.clientTime - (m.brokerDelay / 2)
        else
            m.clientTimeOffset = m.serverTimeMillis - m.clientTime - (m.syncApiDelay / 2)
        end if
        m.template.ops_metadata["server_ts_millis_offset"] = m.clientTimeOffset
    else
        m.template.ops_metadata["server_ts_millis_offset"] = 0
    end if

'    if m.serverTimeTask.serverTimeResult <> invalid and m.serverTimeTask.serverTimeResult.bodystring <> invalid
'        m.syncApiDelay =  m.syncTimer.TotalMilliseconds()
'        if m.serverTimeResponseBody <> "" or m.serverTimeResponseBody <> invalid
'        m.serverTimeResponseBody = parseJSON(m.serverTimeTask.serverTimeResult.bodystring)
'        end if

'    end if
end sub

function apiBus()' 200ms departure
    if m.wsDataTmp.Count() > 0 and m.eventConfirmed = true
        if m.debug = true
            ? "DZ-Print: API BUS JUST DEPARTED WITH "; m.wsDataTmp.Count(); " Events!"
        end if
        for each evnt in m.wsDataTmp
            m.eventJson.Push(ParseJson(evnt))
        end for
        m.sendApiTask = CreateObject("roSGNode", "Task_SendAPI")
        requestData = {}
        requestData.httpMethodString = "POST"
        url = m.brokerUrl
        requestData.urlString = url
        requestData.headersAssociativeArray = { "Content-Type": "application/json" }
        requestData.postBodyString = FormatJson (m.eventJson)
        m.sendApiTask.setField("requestData", requestData)
        m.sendApiTask.observeField("result", "onBrokerResponse")
        m.sendApiTask.control = "RUN"
        m.brokerResponseFired = false
        m.eventConfirmed = false
        m.wsDataTmp = []
        m.eventJson = []
    end if
end function

' Method to send data to REST API
' NEW METHOD

function wsSend(wsData)
    wsDataJson = FormatJson (wsData)
    m.wsDataTmp.Push(wsDataJson)
end function

' NEW METHOD
sub onBrokerResponse()
    if m.brokerResponseFired = false
        if m.sendApiTask.result <> invalid
            if m.sendApiTask.result.bodystring <> invalid and m.sendApiTask.result.bodystring <> ""
                try
                m.brokerResponseBody = ParseJson(m.sendApiTask.result.bodystring)
                catch err
'                   if m.debug = true
                ? "DZ-Print: ERROR "; err
'                   end if
                m.brokerResponseBody.status_code = 505
                end try

                if m.brokerResponseBody.status_code <> invalid
                    try
                    m.apiStatusCode = m.brokerResponseBody.status_code
                    catch err
'                        if m.debug = true
                    ? "DZ-Print: ERROR "; err
'                        end if
                    m.apiStatusCode = 505
                    end try
                else
                    m.apiStatusCode = 505
                end if
            else
                m.apiStatusCode = 505
            end if
            if m.brokerResponseBody <> invalid or m.sendApiTask.result <> invalid
                if m.apiStatusCode >= 500 and m.apiStatusCode < 600 or m.apiStatusCode = 429
                    m.brokerResponseFired = true
                    m.eventConfirmed = false
'            storeEventData(m.wsDataTmp,true)
                    m.brokerResponseFired = true
                    m.eventConfirmed = true
                else if m.apiStatusCode = 400
                    m.eventConfirmed = false
                    m.brokerResponseFired = true
                else
                    m.eventConfirmed = true
                    m.brokerResponseFired = true
'            retrieveEventData()
                end if
            else
                m.brokerResponseFired = true
            end if
        else
'    storeEventData(wsData,true)
            m.sendApiTask.result = invalid
        end if
    end if
end sub

function isArray(array):
    return GetInterface(array, "ifArray") <> invalid
end function

' This method is used to store the event data when needed
function storeEventData(wsData, flag)
    inStore = false

    if m.dzBaseRegister.Exists("eventData")
        dataArray = {}
        dataArray = ParseJson(m.dzBaseRegister.Read("eventData"))
        if dataArray = invalid
            dataArray = {}
        end if
        for each storedKey in dataArray
            if dataArray[storedKey]["event_id"] = wsData["event_id"]
                inStore = true
            end if
        end for
        if inStore = false
            if isArray(wsData)
                for each wsDat in wsData
                    key = getCurrentTimestampInMillis().toStr() + randDigit(3).toStr()
                    dataArray[key] = wsDat
                end for
            else
                key = getCurrentTimestampInMillis().toStr() + randDigit(3).toStr()
                dataArray[key] = wsData
            end if
        end if

        m.dzBaseRegister.Write("eventData", FormatJson(dataArray))
        if flag
'            print "Processed data in storage : Count = "  dataArray.Count()
        end if
    else
        dataArray = {}
        key = getCurrentTimestampInMillis().toStr() + randDigit(3).toStr()

        for each storedKey in dataArray
            if dataArray[storedKey] = wsData
                inStore = true
            end if
        end for

        if inStore = false
            dataArray[key] = wsData
        end if
        m.dzBaseRegister.Write("eventData", FormatJson(dataArray))
        if flag
'            print "Processed data in storage : Count = "  dataArray.Count()
        end if
    end if

end function

function retrieveEventData()
    sendCount = 0
    jsonArray = CreateObject("roArray", 8, true)
    if m.dzBaseRegister.Exists("eventData")
        dataArray = {}
        if m.dzBaseRegister.Read("eventData") <> ""
            dataArray = ParseJson(m.dzBaseRegister.Read("eventData"))
            m.dzBaseRegister.Delete("eventData")
        end if

        if dataArray.Count() > 0
            for each key in dataArray
                jsonArray.push(dataArray[key])
            end for
            m.storedData = jsonArray
        end if
    end if
end function

function purgeEventData()
    jsonArray = CreateObject("roArray", 8, true)
    if m.dzBaseRegister.Exists("eventData")
        dataArray = {}
        if m.dzBaseRegister.Read("eventData") <> ""
            dataArray = ParseJson(m.dzBaseRegister.Read("eventData"))
            m.dzBaseRegister.Delete("eventData")
        end if
        if dataArray.Count() > 0
            for each key in dataArray
                jsonArray.push((dataArray[key]))
            end for
'             wsSend(formatJSON(jsonArray))
'             jsonArray.Clear()
        end if
    end if
end function

' Function to call setSessionTime. This function will be called by both base and collector
function callSetSessionTime()
    setSessionTime()
end function

' Method to check if an event is been configured by customer.
function checkIfEventConfigured(event as String) as Boolean
    m.media_type = getMediaType()
    if m.top.events<> invalid
            for each eventV3 in m.top.events
            if eventV3.name = event
                for each mType in eventV3.media_types
                    if mType = m.media_type or mType = "na"
                        return true
                    end if
                end for
            end if
        end for

    end if
    return false
end function

' Function to check if Fluxdata is available
function checkIfFluxConfigured(event as String) as Boolean
    if m.top.fluxData <> invalid
            for each fluxType in m.top.fluxData
                if fluxType = event
                    return true
                end if
            end for
    end if
    return false
end function

' Function to check if Metadata is available
function checkIfMetaConfigured(event)
        if m.top.metaData <> invalid
            for each metaType in m.top.metaData
                if metaType = event
                    return true
                end if
            end for
        end if
    return false
end function

' Method to get unix epoch time stamp in millisecond 
function getCurrentTimestampInMillis()
    dateObj = CreateObject("roDateTime")
    currentSeconds = dateObj.AsSeconds()
    m& = 1000
    currentMilliseconds = currentSeconds * m&
    ms = currentMilliseconds + dateObj.GetMilliseconds()
    return ms
end function

' Set of functions to get values
' Start
' Returns device's unique ID
function getUniqueDeviceId()
    return CreateObject("roDeviceInfo").getChannelClientId()
end function

function getAdId()
    return CreateObject("roDeviceInfo").GetRIDA()
end function

' Returns Client IP
function getIpAddress()
    return m.geoData.client_ip
end function

' Returns Client city
function getCity()
    return m.geoData.city
end function

' Returns Client ZIP
function getZip()
    return m.geoData.zip
end function

' Returns Client country code
function getCountryCode()
    return m.geoData.countryCode
end function

' Returns Client country
function getCountry()
    return m.geoData.country
end function

' Returns Client latitude
function getLatitude()
    return m.geoData.lat
end function

' Returns Client longitude
function getLongitude()
    return m.geoData.lon
end function

' Returns Client region code
function getRegionCode()
    return m.geoData.regionCode
end function

' Returns Client region
function getRegion()
    return m.geoData.region
end function

function getasn()
    return m.geoData.asn
end function

function getasnOrg()
    return m.geoData.asnOrganization
end function
function getTimezone()
    return m.geoData.timezone
end function
function getTimezoneOffset()
    if GetInterface(m.geoData.timezone_offset, "ifInt") <> invalid
        return m.geoData.timezone_offset / 3600
    else
        return m.geoData.timezone_offset
    end if
end function
function getContinent()
    return m.geoData.continent
end function
function getContinentCode()
    return m.geoData.continentCode
end function
function getDistrict()
    return m.geoData.district
end function

' Returns Client ISP
function getISP()
    return m.geoData.isp
end function

' Returns device OS
function getOS()
    return "Roku OS"
end function

' Returns device OS version
function getOSVersion()
    ver = CreateObject("roDeviceInfo").getOSVersion()
    version = ver.major + "." + ver.minor + "." + ver.revision + "." + ver.build
    return version
end function

' Returns device name
function getModelDisplayName()
    return CreateObject("roDeviceInfo").getModelDisplayName()
end function
' End

' Returns device type
function GetDeviceType()
    return "ott device"
end function
' End

' Returns video type
function getVideoType()
    return "Content"
end function
' End

function getAppSessionID()
    if m.template.user_details.app_session_id = invalid or m.template.user_details.app_session_id = ""
        appSesID = m.Device.getRandomUUID()
        return appSesID
    else
        return m.template.user_details.app_session_id
    end if
end function

function getContentSessionID()
    if m.template.user_details.content_session_id = invalid or m.template.user_details.content_session_id = ""
        contentSesID = m.Device.getRandomUUID()
        return contentSesID
    else
        return m.template.user_details.content_session_id
    end if
end function

function getAssetID()
'    if m.template.video.asset_id = invalid or m.template.video.asset_id = ""
        assetID = m.Device.getRandomUUID()
        return assetID
'    else
'        return m.template.video.asset_id
'    end if
end function
' Function to get random number
function randDigit(count)
    value = ""
    for i = 1 to count
        value += Rnd(9).toStr()
    end for
    return value
end function

' Function to get session ID
'function getSessionData()
'    if m.dzBaseRegister.Exists(m.sessionIdKey) and m.dzBaseRegister.Exists(m.sessionIdTimeKey)
'        if (getCurrentTimestampInMillis() - Val(m.dzBaseRegister.Read(m.sessionIdTimeKey)) < 1260000)
'            sessionIdBool = 1
'            sessionId = m.dzBaseRegister.Read(m.sessionIdKey)
'            return sessionId
'        end if
'    else if sessionID = 0
'        return getAppSessionID()
'    end if
'end function

' function to get sessionStartTimestamp
function getSessionStartTimestamp()
    return m.sessionStartTimestamp
' return Val (m.dzBaseRegister.Read(m.sessionIdTimeKey))
end function

' Function to check session ID
function checkSessionData()
    sessionIdBool = 1
    if m.dzBaseRegister.Exists(m.sessionIdKey) and m.dzBaseRegister.Exists(m.sessionIdTimeKey)
        if (getCurrentTimestampInMillis() - Val(m.dzBaseRegister.Read(m.sessionIdTimeKey)) < 1260000)
            sessionIdBool = 0
        end if
    end if
    if sessionIdBool = 0
        return false
    else
        return true
    end if
end function

' Function to set session ID
function setSessionData()
    if m.videoCntKeyFlag
        sessionId = m.Device.getRandomUUID()
        m.dzBaseRegister.Write(m.sessionIdKey, sessionId)
        m.dzBaseRegister.Write(m.sessionIdTimeKey, getCurrentTimestampInMillis().ToStr())
        if m.dzBaseRegister.Exists(m.vewSessionIdKey)
            m.dzBaseRegister.Delete(m.vewSessionIdKey)
        end if
        if m.dzBaseRegister.Exists(m.noOfVideosKeyName)
            m.dzBaseRegister.Delete(m.noOfVideosKeyName)
        end if
        viewID = m.Device.getRandomUUID()
        m.dzBaseRegister.Write(m.vewSessionIdKey, viewID.ToStr())
        m.setViewIDFlag = false
        return m.dzBaseRegister.Read(m.sessionIdKey)
    else
        return ""
    end if
end function

' Function to set session Time
function setSessionTime()
    if m.dzBaseRegister.Exists(m.sessionIdKey)
        m.dzBaseRegister.Write(m.sessionIdTimeKey, getCurrentTimestampInMillis().ToStr())
        m.dzBaseRegister.Flush()
        return true
    else
        return false
    end if
end function

' Function to session view ID
function setSessionViewId()
    if m.dzBaseRegister.Exists(m.vewSessionIdKey)
        videoCnt = Val(m.dzBaseRegister.Read(m.vewSessionIdKey))
    end if
    videoCnt = m.Device.getRandomUUID()
    m.dzBaseRegister.Write(m.vewSessionIdKey, videoCnt.ToStr())
end function

' Function to get session View ID
function getSessionViewId()
    if m.dzBaseRegister.Exists(m.vewSessionIdKey)
        return m.dzBaseRegister.Read(m.vewSessionIdKey)
    else
        return ""
    end if
end function

' Function to get player ready state
function getPlayerReadyState()
    return m.playerReadyState.toStr()
end function

' Function to set current video URL
function setContentUrlToBase(videoURLCOL)
    m.videoURL = videoURLCOL
'    setNoOfVideos()
'    m.viewIDSet = true
end function

' Function to set number of videos in current session
function setNoOfVideos()
    ba = CreateObject("roByteArray")
    ba.FromAsciiString(m.videoURL)
    getUrl = ba.ToBase64String()
    if getUrl <> ""
        vURL = getUrl
        if m.dzBaseRegister.Exists(m.noOfVideosKeyName)
            nVideoArray = {}
            nVideoArray = ParseJson(m.dzBaseRegister.Read(m.noOfVideosKeyName))
            if nVideoArray[vURL] <> invalid
                m.noOfVid = nVideoArray.Count()
            else
                nVideoArray[vURL] = getCurrentTimestampInMillis().ToStr()
                m.dzBaseRegister.Write(m.noOfVideosKeyName, FormatJson(nVideoArray))
                m.noOfVid = nVideoArray.Count()
                setSessionViewId()
            end if
        else
            nVideoArray = {}
            nVideoArray[vURL] = getCurrentTimestampInMillis().ToStr()
            m.dzBaseRegister.Write(m.noOfVideosKeyName, FormatJson(nVideoArray))
            m.noOfVid = 1
        end if
    end if
end function

' Function to get number of video during current session
function getNoOfVideos()
    return m.noOfVid.toStr()
end function
function getConnectionType()
    return CreateObject("roDeviceInfo").getConnectionType()
end function
